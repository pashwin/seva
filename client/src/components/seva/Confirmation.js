import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { formatCurrency } from '../../utils/currencyUtils';

class Confirmation extends Component {
  render() {
    const { booking } = this.props;

    const {
      seva: { name: sevaName },
      organization: { name: organizationName },
      userDetails: {
        name: userName,
        nakshatra,
        rashi,
        messageToPriest,
        dob
      },
      paymentStatus: { amount }
    } = booking;

    return (
      <Container>
        <div className="confirmation">
          <h3>Confirmation</h3>
          <div className="text-muted">Your seva has been booked successfully</div>
          <div style={{ height: '16px' }}></div>
          <div className="title">{organizationName}</div>
          <div className="text-muted">Organization</div>
          <hr />
            <div className="title">{sevaName}</div>
            <div className="price">{formatCurrency(amount)}</div>
            <div className="text-muted">Seva details</div>
            <hr />
            <div className="sub-title">User details</div>
            <div className="sub-details">
              <Row>
                <Col xs="12" className="key">Name</Col>
                <Col xs="12" className="value"><div>{userName}</div></Col>
              </Row>
              <Row>
                <Col xs="12" className="key">Nakshatra</Col>
                <Col xs="12" className="value"><div>{nakshatra}</div></Col>
              </Row>
              <Row>
                <Col xs="12" className="key">Rashi</Col>
                <Col xs="12" className="value"><div>{rashi}</div></Col>
              </Row>
              <Row>
                <Col xs="12" className="key">MessageToPriest</Col>
                <Col xs="12" className="value"><div>{messageToPriest}</div></Col>
              </Row>
              <Row>
                <Col xs="12" className="key">Date of birth</Col>
                <Col xs="12" className="value"><div>{dob}</div></Col>
              </Row>
              <Row>
                <Col xs="12" className="key"></Col>
                <Col xs="12" className="value"></Col>
              </Row>
              <Row>
                <Col xs="12" className="key"></Col>
                <Col xs="12" className="value"></Col>
              </Row>
              <Row>
                <Col xs="12" className="key"></Col>
                <Col xs="12" className="value"></Col>
              </Row>
            </div>
        </div>
      </Container>
    );
  }
}
export default Confirmation;
