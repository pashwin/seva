import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container, Button, Form, FormGroup, FormFeedback, Label, Input } from 'reactstrap';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import {
  fetchOrganizations,
  fetchSevas,
  onOrganizationValueChange,
  saveOrganization,
  saveSeva,
  deleteSeva
} from './actionsAdmin';
import AdminSevas from './AdminSevas';
import AdminBookings from './AdminBookings';
import IFrameEmbedInfo from './IFrameEmbedInfo';
import { withRouter } from 'react-router';
import _get from 'lodash/get';
import _keys from 'lodash/keys';
import _each from 'lodash/each';

const mapStateToProps = (state, ownProps) => {
  return {
    admin: state.admin
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchOrganizations,
    onOrganizationValueChange,
    fetchSevas,
    saveOrganization,
    saveSeva,
    deleteSeva
  }, dispatch);
};

class Admin extends Component {
  state = {
    name: '',
    address: '',
    validationErrors: {},
    activeTab: '1'
  }

  componentDidMount() {
    this.props.fetchOrganizations()
      .then((organizationData) => {
        const organizationId = _get(organizationData, 'data._id');
        if (organizationId) {
          this.props.fetchSevas(organizationId);
        }
      })
      .catch((error) => {
        console.log('error ', error);
      });
  }

  validateOrganizationName = () => {
    const { admin } = this.props;
    const name = _get(admin, 'organization.name', '');
    if (!name) {
      return {
        valid: false,
        message: 'Name is required'
      }
    }

    if (name.length > 500 ) {
      return {
        valid: false,
        message: 'Name is too long'
      }
    }
  }

  validateOrganizationAddress = () => {
    const { admin } = this.props;
    const name = _get(admin, 'organization.address', '');
    if (!name) {
      return {
        valid: false,
        message: 'Addess is required'
      }
    }
  }

  isFormValid = (validationErrors) => {
    let hasErrors = false;
    const keys = _keys(validationErrors);
    _each(keys, (key) => {
      if (validationErrors[key]) {
        hasErrors = true;
      }
    });
    return !hasErrors;
  }

  saveOrganization = (e) => {
    e.preventDefault();
    const validationErrors = {};
    validationErrors.name = this.validateOrganizationName();
    validationErrors.address = this.validateOrganizationAddress();

    const isFormValid = this.isFormValid(validationErrors);

    this.setState({
      validationErrors,
      isFormValid
    });

    if (isFormValid) {
      const organizationDetails = _get(this.props, 'admin.organization');
      this.props.saveOrganization(organizationDetails);
    }

  }

  onOrganizationValueChange = (event, fieldName) => {
    this.props.onOrganizationValueChange(event.target.value, fieldName);
  }

  renderOrganizationSection = () => {
    const organization = _get(this.props, 'admin.organization') || {};
    const {
      name,
      address
    } = organization;

    const validationErrors = this.state.validationErrors;
    const isNameValid = _get(validationErrors, 'name.valid', true);
    const isAddressValid = _get(validationErrors, 'address.valid', true);

    return (
      <div>
        <Form>
          <h3>Temple Details</h3>
          <FormGroup>
            <Label for="organization">Name</Label>
            <Input
              value={name}
              onChange={(event) => { this.onOrganizationValueChange(event, 'name')}}
              innerRef={(input) => (this.organizationName = input)}
              type="text"
              name="organization"
              id="organization"
              placeholder="Eg: Sri Krishna Temple"
              invalid={!isNameValid}
            />
          <FormFeedback>Name is required</FormFeedback>
          </FormGroup>
          <FormGroup>
            <Label for="organization">Address</Label>
            <Input
              value={address}
              onChange={(event) => { this.onOrganizationValueChange(event, 'address')}}
              innerRef={(input) => (this.organizationAddress = input)}
              type="text"
              name="address"
              id="address"
              placeholder="2211 N 1st street, CA, USA"
              invalid={!isAddressValid}
            />
          <FormFeedback>Address is required</FormFeedback>
          </FormGroup>
          <Button color='danger' onClick={this.saveOrganization}>Save</Button>
        </Form>
      </div>
    )
  }

  saveSeva = (sevaDetails) => {
    const organization = _get(this.props, 'admin.organization');
    return this.props.saveSeva(organization._id, sevaDetails)
      .then((results) => {
        console.log('results', results);
      })
      .catch((error) => {
        console.log('error ', error);
      });
  }

  deleteSeva = (sevaDetails) => {
    const organization = _get(this.props, 'admin.organization');
    return this.props.deleteSeva(organization._id, sevaDetails)
      .then((results) => {
        console.log('results', results);
      })
      .catch((error) => {
        console.log('error ', error);
      });
  }

  renderSevasSection = () => {
    const organization = _get(this.props, 'admin.organization');
    if (!_get(organization, '_id', undefined)) {
      return null;
    }

    return (
      <div className="sevas">
        <AdminSevas
          sevas={this.props.admin.sevas}
          organization={organization}
          saveSeva ={this.saveSeva}
          deleteSeva={this.deleteSeva}
        />
      </div>
    )
  }

  renderiFrameSection = () => {
    const organization = _get(this.props, 'admin.organization');
    if (!_get(organization, '_id', undefined)) {
      return null;
    }

    return (
      <IFrameEmbedInfo
        organizationId={organization._id}
      />
    )
  }

  renderBookingsTab = () => {
    const organization = _get(this.props, 'admin.organization');
    if (!_get(organization, '_id', undefined)) {
      return null;
    }

    return (
      <AdminBookings
        organization={organization}
      />
    )
  }

  toggleTab = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    const organization = _get(this.props, 'admin.organization');

    return (
      <Container>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={`${this.state.activeTab === '1' ? 'active' : ''}`}
              onClick={() => { this.toggleTab('1'); }}
            >
              Settings
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={`${this.state.activeTab === '2' ? 'active' : ''}`}
              onClick={() => { this.toggleTab('2'); }}
            >
              Bookings
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            {this.renderOrganizationSection()}
            <div className="section-divider">
              <span>Next</span>
            </div>
            {this.renderSevasSection()}
            {this.renderiFrameSection()}
          </TabPane>
          <TabPane tabId="2">
            {this.renderBookingsTab()}
          </TabPane>
        </TabContent>


      </Container>
    );
  }
}

const AdminWithRouter = withRouter(Admin);
export default connect(mapStateToProps, mapDispatchToProps)(AdminWithRouter);
