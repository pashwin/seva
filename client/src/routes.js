import React from 'react';
import { Switch, Route } from 'react-router';
import App from './containers/App';
import HomePage from './containers/HomePage';
import Login from './components/auth/Login';
import Signup from './components/auth/Signup';
import OnlineSeva from './components/seva/OnlineSeva';
import Launcher from './iframeLauncher/Launcher';
import Admin from './components/admin/Admin';
import PrivateRoute from './components/common/router/PrivateRoute';

export default () => (
  <App>
    <Switch>
      <Route path="/launcher" component={Launcher} />
      <Route path="/login" component={Login} />
      <Route path="/signup" component={Signup} />
      <Route exact path="/organizations/:organizationId/bookings" component={OnlineSeva} />
      <Route exact path="/bookings" component={OnlineSeva} />
      <Route exact path="/bookings/:bookingId" component={OnlineSeva} />
      <PrivateRoute path="/admin" component={Admin} />
      <Route path="/" component={HomePage} />
    </Switch>
  </App>
);
