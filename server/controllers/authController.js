const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const User = require('../models/User');
import { createErrorString } from '../helpers/validationErrors';
import { generateToken } from '../helpers/authHelpers';

// var oauth2Client = new OAuth2(
//   process.env.TABLE_FACTORY_GOOGLE_CLIENT_ID,
//   process.env.TABLE_FACTORY_GOOGLE_SECRET,
//   process.env.TABLE_FACTORY_REDIRECT_URL
// );

// Set user info from request
function setUserInfo(request) {
  return {
    _id: request._id,
    email: request.email
    // name: request.name
    // role: request.role,
    // firstName: request.profile.firstName,
    // lastName: request.profile.lastName,
  };
}

exports.login = function (req, res, next) {
  let userInfo = setUserInfo(req.user);

  res.status(200).json({
    token: 'Bearer ' + generateToken(userInfo),
    user: userInfo
  });
}

exports.register = function (req, res, next) {
  // Check for registration errors
  const email = req.body.email;
  // const name = req.body.name;
  const password = req.body.password;

  // Return error if no email provided
  if (!email) {
    return res.status(422).send(createErrorString('EMAIL_REQUIRED', 'You must enter an email address.'));
  }
  // Return error if no password provided
  if (!password) {
    return res.status(422).send(createErrorString('PASSWORD_REQUIRED', 'You must enter a password.'));
  }

  User.findOne({ email: email }, function(err, existingUser) {
      if (err) {
        return next(err);
      }

      // If user is not unique, return error
      if (existingUser) {
        return res.status(422).send(createErrorString('EMAIL_IN_USE', 'That email address is already in use.'));
      }

      // If email is unique and password was provided, create account
      let user = new User({
        email: email,
        password: password
        // ,
        // profile: { firstName: firstName, lastName: lastName }
      });

      user.save(function(err, user) {
        if (err) {
          return next(err);
        }

        // Subscribe member to Mailchimp list
        // mailchimp.subscribeToNewsletter(user.email);

        // Respond with JWT if user was created
        let userInfo = setUserInfo(user);
        const token = generateToken(userInfo);

        res.status(201).json({
          token: 'Bearer ' + token,
          user: userInfo
        });
      });
  });
}


