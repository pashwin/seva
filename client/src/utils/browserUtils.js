import _debounce from 'lodash/debounce';

window.addEventListener('resize', _debounce(calculateViewportSize, 300));

let windowWidth;
let windowHeight;

export function calculateViewportSize() {
  const width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  const height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

  windowWidth = width;
  windowHeight = height;
}

export function getViewportSize() {
  if (windowWidth && windowHeight) {
    return {
      width: windowWidth,
      height: windowHeight
    }
  } else {
    calculateViewportSize();
    return getViewportSize();
  }
}

export function isXSDevice() {
  return getViewportSize().width <= 480;
}

export function isSMDevice() {
  return getViewportSize().width <= 768;
}

export function isMDDevice() {
  return getViewportSize().width <= 992;
}

export function isLGDevice() {
  return getViewportSize().width <= 1200;
}