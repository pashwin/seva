var database = {
  dbHost: '0.0.0.0',
  dbName: 'temples_seva'
};

const dbHost = database.dbHost;
const dbName = database.dbName;
const mongoDbUrl = `mongodb://${dbHost}/${dbName}`;

console.log('dbHost ->', dbHost);
console.log('dbName ->', dbName);

// Mongoose
const mongoose = require('mongoose');
mongoose.Promise = Promise;

var _db;

module.exports = {

  connectMongoose: function() {
    return mongoose.connect(mongoDbUrl);
  },

  getDb: function() {
    return _db;
  }
};
