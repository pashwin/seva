import { generateToken } from '../helpers/authHelpers';


exports.getLoggedInUser = function(req, res, next) {
  if (req.user) {
    res.send({ user: req.user });
  } else {
    res.send({ user: null });
  }
}