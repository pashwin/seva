import Cookies from 'js-cookie';
import {
  getOptions,
  postOptions
} from '../../api/fetchOptions';

export const AUTH_LOGIN_REQUEST = 'AUTH_LOGIN_REQUEST';
export const AUTH_LOGIN_SUCCESS = 'AUTH_LOGIN_SUCCESS';
export const AUTH_LOGIN_ERROR = 'AUTH_LOGIN_ERROR';
export const AUTH_REGISTER_REQUEST = 'AUTH_REGISTER_REQUEST';
export const AUTH_REGISTER_SUCCESS = 'AUTH_REGISTER_SUCCESS';
export const AUTH_REGISTER_ERROR = 'AUTH_REGISTER_ERROR';
export const AUTH_LOGOUT_SUCCESS = 'AUTH_LOGOUT_SUCCESS';
export const AUTH_LOGOUT_ERROR = 'AUTH_LOGOUT_ERROR';
export const AUTH_BUSY = 'AUTH_BUSY';

export const AUTH_RETRIEVEUSER_SUCCESS = 'AUTH_RETRIEVEUSER_SUCCESS';
export const AUTH_RETRIEVEUSER_ERROR = 'AUTH_RETRIEVEUSER_ERROR';

const authBusy = () => ({ type: AUTH_BUSY });

// const requestLogin = () => ({ type: AUTH_LOGIN_REQUEST });
const loginSuccess = authInfo => ({ type: AUTH_LOGIN_SUCCESS, payload: authInfo });
const loginError = (error) => ({ type: AUTH_LOGIN_ERROR, payload: { error: error } });

export const login = (email, password) => (dispatch, getState) => {
  dispatch(authBusy());

  const url = `/api/auth/login`;

  return fetch(url, {
    ...postOptions,
    body: JSON.stringify({ email, password })
  }).then(response =>  {
      if (!response.ok) {
        return response.json().then(err => {throw err});
      }
      return response.json()
    })
    .then((response) => {
      // path: '/' Default
      Cookies.set('token', response.token, { expires: 365 });

      return response;
    })
    .then(authInfo => dispatch(loginSuccess(authInfo)))
    .catch((err) => {
      console.log('err', err);
      return dispatch(loginError(err))
    });
};

// const requestRegister = () => ({ type: AUTH_REGISTER_REQUEST });
const registerSuccess = authInfo => ({ type: AUTH_REGISTER_SUCCESS, payload: authInfo });
const registerError = (error) => ({ type: AUTH_REGISTER_ERROR, payload: { error: error } });

export const register = (email, password) => (dispatch, getState) => {
  dispatch(authBusy());

  const url = `/api/auth/register`;
  // SEARCH
  return fetch(url, {
    ...postOptions,
    body: JSON.stringify({ email, password })
  }).then(response =>  {
    if (!response.ok) {
      return response.json().then(err => {throw err});
    }
    return response.json()
  })
  .then((response) => {
      // path: '/' Default
      Cookies.set('token', response.token, { expires: 365 });

    return response;
  })
  .then(authInfo => dispatch(registerSuccess(authInfo)))
  .catch((err) => {
    console.log('err', err);
    dispatch(registerError(err))
  });
};

const logoutSuccess = () => ({ type: AUTH_LOGOUT_SUCCESS });
// const logoutError = () => ({ type: AUTH_LOGOUT_ERROR });

export const logout = () => (dispatch, getState) => {
  Cookies.remove('token', { path: '/' });

  dispatch(logoutSuccess());

  window.location.href = '/';

}

const retrieveUserSuccess = userInfo => ({ type: AUTH_RETRIEVEUSER_SUCCESS, payload: userInfo });
const retrieveUserError = (error) => ({ type: AUTH_RETRIEVEUSER_ERROR, payload: { error }})

export const getLoggedInUser = () => (dispatch, getState) => {
  dispatch(authBusy());
  const url = '/api/user/getLoggedInUser';

  return fetch(url, getOptions)
  .then(response =>  {
    if (!response.ok) {
      return response.json().then(err => {throw err});
    }
    return response.json()
  })
  .then(userInfo => dispatch(retrieveUserSuccess(userInfo)))
  .catch((err) => {
    console.log('err', err);
    dispatch(retrieveUserError(err))
  });
}