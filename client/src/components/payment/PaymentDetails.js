import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormFeedback } from 'reactstrap';
import GlobalError from '../common/error/GlobalError';
import _has from 'lodash/has';
import _keys from 'lodash/keys';
import _get from 'lodash/get';
import _each from 'lodash/each';
import {
  // CardElement,
  CardNumberElement,
  CardExpiryElement,
  CardCVCElement,
  // PostalCodeElement,
  // PaymentRequestButtonElement,
  StripeProvider,
  Elements,
  injectStripe,
} from 'react-stripe-elements';
import Collapsible from '../common/collapsible/Collapsible';
import { formatCurrency } from '../../utils/currencyUtils';
import constants from '../../constants';

const handleBlur = () => { };
const handleChange = change => { };
// const handleClick = () => { };
const handleFocus = () => { };
const handleReady = () => { };

const createOptions = (fontSize) => {
  return {
    style: {
      base: {
        fontSize,
        color: '#424770',
        letterSpacing: '0.025em',
        fontFamily: 'Source Code Pro, Menlo, monospace',
        '::placeholder': {
          color: '#aab7c4',
        },
      },
      invalid: {
        color: '#9e2146',
      },
    },
  };
};

class _SplitForm extends React.Component {
  state = {
    cardNumber: '',
    cardExpiry: '',
    cardSecurityCode: '',
    streetAddress: '',
    addresLine2: '',
    city: '',
    state: '',
    zip: '',
    formFields: {
      name: {}
    },
    validationErrors: {},
    globalError: {}
  }

  componentDidMount() {
    const { paymentStatus } = this.props;
    const errorMessage = _get(paymentStatus, 'error.message');

    if (errorMessage) {
      this.setState({
        globalError: {
          message: errorMessage
        }
      });
    }
  }

  componentWillReceiveProps(newProps) {
    const { paymentStatus } = newProps;
    const errorMessage = _get(paymentStatus, 'error.message');

    if (errorMessage) {
      this.setState({
        globalError: {
          message: errorMessage
        }
      });
    }
  }

  onChange = (fieldName, event) => {
    this.setState({
      [fieldName]: event.target.value
    })
  }

  validateName = () => {
    if (!this.state.name) {
      return {
        valid: false,
        message: 'Name is required'
      }
    }
  }

  validateStreetAddress = () => {
    if (!this.state.streetAddress) {
      return {
        valid: false,
        message: 'Address is required'
      }
    }
  }
  validateCity = () => {
    if (!this.state.city) {
      return {
        valid: false,
        message: 'City is required'
      }
    }
  }
  validateState = () => {
    if (!this.state.state) {
      return {
        valid: false,
        message: 'State is required'
      }
    }
  }
  validateZip = () => {
    if (!this.state.zip) {
      return {
        valid: false,
        message: 'Zip is required'
      }
    }
  }
  isFormValid = (validationErrors) => {
    let hasErrors = false;
    const keys = _keys(validationErrors);
    _each(keys, (key) => {
      if (validationErrors[key]) {
        hasErrors = true;
      }
    });
    return !hasErrors;
  }

  handleSubmit = (e) => {
    this.setState({
      globalError: {}
    });
    e.preventDefault();

    const validationErrors = {};
    validationErrors.name = this.validateName();
    validationErrors.streetAddress = this.validateStreetAddress();
    validationErrors.city = this.validateCity();
    validationErrors.state = this.validateState();
    validationErrors.zip = this.validateZip();

    const isFormValid = this.isFormValid(validationErrors)

    this.setState({
      validationErrors,
      isFormValid
    });

    if (isFormValid) {
      const stripeCardData = {
        name: this.state.name,
        address_line1: this.state.streetAddress,
        address_line2: this.state.addressLine2,
        address_city: this.state.city,
        address_state: this.state.state,
        address_zip: this.state.zip
      }

      const promise = this.props.stripe.createToken(stripeCardData);
      promise
        .then((response) => {
          const { token, error } = response;
          if (error) {
            this.setState({
              globalError: error
            })
          } else if (token) {
            this.sendValuesToParentComponent(token, stripeCardData);
          }
        }).catch( (error) => {
          console.log('error ', error);
          throw error;
        });
    }
  }

  sendValuesToParentComponent(token, stripeCardData) {
    this.props.saveStripeToken(token, stripeCardData)
      .then((results) => {
      })
      .catch((error) => {
        console.log('error ', error);
        const errorMessage = _get(error, 'error.message')
        this.setState({
          globalError: {
            message: errorMessage
          }
        })
      });
  }

  isFieldValid = (fieldName) => {
    if (_has(this.state.validationErrors, fieldName)) {
      return !this.state.validationErrors[fieldName];
    }

    return true;
  }

  handleFocus = (e, inputName) => {
    const validationErrors = this.state.validationErrors;
    validationErrors[inputName] = undefined;
    this.setState({
      validationErrors
    }, () => {
    });

  }

  render() {
    const errorString = _get(this.state, 'globalError.message');
    const chargeAmount = this.props.chargeAmount;

    return (
      <Form onSubmit={this.handleSubmit}>
        {
          errorString && <div style={{paddingBottom: '8px'}}><GlobalError error={errorString} /></div>
        }

        <FormGroup>
          <Label for="name" hidden>Card holder's name</Label>
          <Input
            type="text"
            name="name"
            id="name"
            placeholder="Card holder's name"
            onFocus={(e) => { this.handleFocus(e, 'name') }}
            onChange={(e) => this.onChange('name', e)}
            invalid={!this.isFieldValid('name')}
          />
          {
            !this.isFieldValid('name') && <FormFeedback invalid={true}>Name is required</FormFeedback>
          }
        </FormGroup>

        <FormGroup>
          <CardNumberElement
            onBlur={handleBlur}
            onChange={handleChange}
            onFocus={handleFocus}
            onReady={handleReady}
            {...createOptions(this.props.fontSize)}
          />
        </FormGroup>
        <FormGroup>
          <CardExpiryElement
            onBlur={handleBlur}
            onChange={handleChange}
            onFocus={handleFocus}
            onReady={handleReady}
            {...createOptions(this.props.fontSize)}
          />
        </FormGroup>
        <FormGroup>
          <CardCVCElement
            onBlur={handleBlur}
            onChange={handleChange}
            onFocus={handleFocus}
            onReady={handleReady}
            {...createOptions(this.props.fontSize)}
          />
        </FormGroup>

          {/* Address */}
          <FormGroup>
            <Label for="streetAddress">Billing Address</Label>
            <Input
              type="text"
              name="streetAddress"
              id="streetAddress"
              placeholder="Street Address"
              onFocus={(e) => { this.handleFocus(e, 'streetAddress') }}
              onChange={(e) => this.onChange('streetAddress', e)}
              invalid={!this.isFieldValid('streetAddress')}
            />
            {
              !this.isFieldValid('streetAddress') && <FormFeedback invalid={true}>Billing Address is required</FormFeedback>
            }
          </FormGroup>

          <FormGroup>
            <Input
              type="text"
              name="addressLine2"
              id="addressLine2"
              placeholder="Apt., Street, Bldg (Optional)"
              onFocus={(e) => { this.handleFocus(e, 'addressLine2') }}
              onChange={(e) => this.onChange('addressLine2', e)}
              invalid={!this.isFieldValid('addressLine2')}
            />
            {
              !this.isFieldValid('addressLine2') && <FormFeedback invalid={true}>Please enter the card expiry date</FormFeedback>
            }
          </FormGroup>
          <div className="row">
            <div className="col-md-4 col-sm-12">
              <FormGroup>
                <Label for="city" hidden>City</Label>
                <Input
                  type="text"
                  name="city"
                  id="city"
                  placeholder="City"
                  onFocus={(e) => { this.handleFocus(e, 'city') }}
                  onChange={(e) => this.onChange('city', e)}
                  invalid={!this.isFieldValid('city')}
                />
                {
                  !this.isFieldValid('city') && <FormFeedback invalid={true}>City is required</FormFeedback>
                }
              </FormGroup>
            </div>
            <div className="col-md-4 col-sm-12">
              <FormGroup>
                <Label for="state" hidden>State</Label>
                <Input
                  type="text"
                  name="state"
                  id="state"
                  placeholder="State"
                  onFocus={(e) => { this.handleFocus(e, 'state') }}
                  onChange={(e) => this.onChange('state', e)}
                  invalid={!this.isFieldValid('state')}
                />
                {
                  !this.isFieldValid('state') && <FormFeedback invalid={true}>State is required</FormFeedback>
                }
              </FormGroup>
            </div>
            <div className="col-md-4 col-sm-12">
              <FormGroup>
                <Label for="zip" hidden>Zip</Label>
                <Input
                  type="text"
                  name="zip"
                  id="zip"
                  placeholder="Zip"
                  onFocus={(e) => { this.handleFocus(e, 'zip') }}
                  onChange={(e) => this.onChange('zip', e)}
                  invalid={!this.isFieldValid('zip')}
                />
                {
                  !this.isFieldValid('zip') && <FormFeedback invalid={true}>Zip is required</FormFeedback>
                }
              </FormGroup>
            </div>
          </div>
          <Button
            color="primary"
            size="lg"
            block
            onClick={this.handleSubmit}
          >
            Pay {formatCurrency(chargeAmount)}
          </Button>

      </Form>
    );
  }
}
const SplitForm = injectStripe(_SplitForm);

class Checkout extends React.Component {
  constructor() {
    super();
    this.state = {
      elementFontSize: window.innerWidth < 450 ? '14px' : '14px',
    };

    window.addEventListener('resize', () => {
      if (window.innerWidth < 450 && this.state.elementFontSize !== '14px') {
        this.setState({elementFontSize: '14px'});
      } else if (
        window.innerWidth >= 450 &&
        this.state.elementFontSize !== '14px'
      ) {
        this.setState({ elementFontSize: '14px' });
      }
    });
  }

  render() {
    const {elementFontSize} = this.state;
    return (
      <div className="Checkout">
        <Elements>
          <SplitForm fontSize={elementFontSize} {...this.props}/>
        </Elements>
      </div>
    );
  }
}

class App extends React.Component {

  renderPaymentForm = () => {
    return (
      <div className="payment-details-container">
        <StripeProvider apiKey={constants.STRIPE.API_KEY}>
          <Checkout {...this.props}/>
        </StripeProvider>
      </div>
    )
  }

  renderCollapsedView = () => {
    const amount = _get(this.props, 'paymentStatus.amount')
    return (
      <div>
        Thank you for the payment of {amount}
      </div>
    )
  }

  render() {
    let collapsed = this.props.collapsed;

    if (_get(this.props, 'paymentStatus.status') === 'CHARGED_SUCCESSFULLY') {
      collapsed = true;
    };

    return (
      <Collapsible
        title="Payment Details"
        stepNumber="3"
        collapsed={collapsed}
        expandedComponent={this.renderPaymentForm()}
        collapsedComponent={this.renderCollapsedView()}
        onExpand={() => this.props.expandCurrentAction('paymentDetails')}
      >
      </Collapsible>
    );
  }
};

export default App;
