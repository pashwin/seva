import {
  getOptions,
  postOptions,
  putOptions
} from '../../api/fetchOptions';
import constants from '../../constants';

export const SEVA_BUSY = 'SEVA_BUSY';
export const FETCH_SEVAS_SUCCESS = 'FETCH_SEVAS_SUCCESS';
export const FETCH_SEVAS_ERROR = 'FETCH_SEVAS_ERROR';
// export const INIT_ONLINE_SEVA = 'INIT_ONLINE_SEVA';
export const SELECT_SEVA = 'SELECT_SEVA';
export const SET_ACTIVE_MENU_ITEM = 'SET_ACTIVE_MENU_ITEM';
export const SAVE_SEVA_DETAILS = 'SAVE_SEVA_DETAILS';
export const SET_BOOKING_IN_VIEW = 'SET_BOOKING_IN_VIEW';

export const SET_COLLAPSED_MENU_ITEM = 'SET_COLLAPSED_MENU_ITEM';
export const CONTINUE_TO_NEXT_MENU = 'CONTINUE_TO_NEXT_MENU';
export const EXPAND_CURRENT_MENU = 'EXPAND_CURRENT_MENU';

export const menuItems = ['sevaList', 'sevaDetails', 'paymentDetails' ];


// Temp code. @TODO: Remove this
const ORGANIZATION_ID = constants.ORGANIZATION_ID;


const busy = () => ({ type: SEVA_BUSY })

export const fetchSevas = () => {
  return (dispatch, getState) => {
    dispatch(busy());

    const url = `/api/organizations/${ORGANIZATION_ID}/sevas`;
    return fetch(url, {
      ...getOptions
    })
    .then((response) => {
      if (!response.ok) {
        return response.json().then(err => {throw err});
      }
      return response.json();
    })
    .then((sevas) => {
      dispatch({
        type: FETCH_SEVAS_SUCCESS,
        payload: sevas
      })
      return sevas;
    })
    .catch( (error) => {
      dispatch({
        type: FETCH_SEVAS_ERROR
      });
    });
  }

}

export function setActiveMenuItem(menuName) {
  return {
    type: SET_ACTIVE_MENU_ITEM,
    payload: menuName
  }
}

export function selectSeva(seva) {
  return {
    type: SELECT_SEVA,
    payload: seva
  }
}

export function saveSevaUserDetails(organization, sevaUserDetails) {
  return {
    type: SAVE_SEVA_DETAILS,
    payload: sevaUserDetails
  }
}

export function setBookingInView(booking) {
  return {
    type: SET_BOOKING_IN_VIEW,
    payload: booking
  }
}

export function fetchBooking(organizationId, bookingId) {
  return (dispatch, getState) => {
    dispatch(busy());

    if (!bookingId) {
      return Promise.resolve();
    };

    const url = `/api/organizations/${organizationId}/bookings/${bookingId}`;

    return fetch(url, {
      ...getOptions
    })
    .then((response) => {
      if (!response.ok) {
        return response.json().then(err => {throw err});
      }
      return response.json();
    })
    .then((response) => {
      const savedBooking = response.data;
      dispatch(setBookingInView(savedBooking))
      return response;
    })
    .catch( (error) => {
      console.log('error ', error);
      throw error;
    });
  }
}

export function saveBooking(organization, booking) {
  return (dispatch, getState) => {
    dispatch(busy());

    let url;
    const bookingId = booking._id;
    const organizationId = organization._id;
    if (bookingId) {
      url = `/api/organizations/${organizationId}/bookings/${bookingId}`;
    } else {
      url = `/api/organizations/${organizationId}/bookings`;
    }

    return fetch(url, {
      ...(bookingId ? putOptions : postOptions),
      body: JSON.stringify({
        booking: booking
      })
    })
    .then((response) => {
      if (!response.ok) {
        return response.json().then(err => {throw err});
      }
      return response.json();
    })
    .then((response) => {
      const savedBooking = response.data;
      dispatch(setBookingInView(savedBooking))
      return response;
    })
    .catch( (error) => {
      console.log('error ', error);
      throw error;
    });
  }
}

export function saveStripeToken(organizationId, bookingId, token, stripeCardData) {
  return (dispatch, getState) => {
    dispatch(busy());

    const url = `/api/organizations/${organizationId}/bookings/${bookingId}/charge`;

    return fetch(url, {
      ...postOptions,
      body: JSON.stringify({
        token,
        stripeCardData,
        amount: 100
      })
    })
    .then((response) => {
      if (!response.ok) {
        return response.json().then(err => {throw err});
      }
      return response.json();
    })
    .then((response) => {
      return response;
    })
    .catch( (error) => {
      console.log('error ', error);
      throw error;
    });
  }
}

export function continueToNextMenuItem(menuName) {
  return {
    type: CONTINUE_TO_NEXT_MENU,
    payload: {
      menuName
    }
  }
}

export function expandCurrentAction(menuName) {
  return {
    type: EXPAND_CURRENT_MENU,
    payload: {
      menuName
    }
  }
}

export function setCollapsedMenuItem(menuName) {
  return {
    type: SET_COLLAPSED_MENU_ITEM,
    payload: {
      menuName
    }
  }
}
