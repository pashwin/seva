var Seva = require('../models/Seva.js');
var Organization = require('../models/Organization');
const _ = require('lodash');

async function sleep(ms = 0) {
  return new Promise(r => setTimeout(r, ms));
}

exports.list = async (req, res, next) => {
  const organizationId = req.params.organizationId;
  try {
    const sevas = await Seva.find({
      organization: organizationId
    })
    .sort({ updatedAt: '-1' })
    .populate('organization')
    .exec();

    res.send({
      data: sevas
    });

  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.get = async (req, res, next) => {
  const sevaId = req.params.id;

  try {
    const seva = await Seva.findById(sevaId)
      .populate('organization')
      .exec();

    res.send({
      data: seva
    });


  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.createOrUpdate = async (req, res, next) => {
  const seva = req.body.seva;
  const organizationId = _.get(req, 'params.organizationId');

  if (!organizationId) {
    res.status(500).send({
      error: {
        code: 'SEVA_REQUIRED'
      }
    })
    return;
  }

  try {
    const organization = await Organization.findById(organizationId);
    if (!organization) {
      res.status(500).send({
        error: {
          code: 'INVALID_ORGANIZATION_ID'
        }
      })
      return;
    };

    const sevaToSave = {
      organization: organizationId,
      ...seva
    };
    delete sevaToSave._id;

    const sevaId = seva._id;
    var createdSeva;

    if (sevaId) {
      // UPDATE
      createdSeva = await Seva.findOneAndUpdate(
        { _id: sevaId },
        {
          ...sevaToSave
        },
        { new: true, overwrite: true }
      );
    } else {
      // CREATE
      createdSeva = await Seva.create(sevaToSave);
    }
    await createdSeva.populate('organization').execPopulate();

    res.send({
      data: createdSeva
    });

  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.post = async (req, res, next) => {
  const seva = req.body.seva;
  console.log('req.body', req.body);
  console.log('seva', seva);

  try {
    const createdSeva = await Seva.create(seva)
      .populate('organization')
      .exec();

    res.send({
      data: createdSeva
    });

  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.remove = async (req, res, next) => {
  const id = req.params.sevaId;

  try {
    const seva = await Seva.findByIdAndRemove(id)
      .populate('organization')
      .exec();

    res.send({
      data: seva
    });
  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}











