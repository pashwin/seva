const _ = require('lodash');
const createCharge = require('../utils/stripeUtils').createCharge;
const Booking = require('../models/Booking');
const Seva = require('../models/Seva');


exports.list = async (req, res, next) => {
  const organizationId = req.params.organizationId;
  try {
    const bookings = await Booking.find({
      organization: organizationId
    })
      .sort({ createdAt: '-1' })
      .populate('seva')
      .populate('organization')
      .exec();

    res.send({
      data: bookings
    });

  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.paidBookings = async (req, res, next) => {
  const organizationId = req.params.organizationId;
  try {
    const bookings = await Booking.find({
      organization: organizationId,
      'paymentStatus.status': 'CHARGED_SUCCESSFULLY'
    })
      .sort({ updatedAt: '-1' })
      .populate('seva')
      .populate('organization')
      .exec();

    res.send({
      data: bookings
    });

  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.get = async (req, res, next) => {
  const bookingId = req.params.bookingId;
  console.log('bookingId', bookingId);

  try {
    const booking = await Booking.findById(bookingId)
      .populate('seva')
      .populate('organization')
      .exec();

    res.send({
      data: booking
    });


  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.createOrUpdate = async (req, res, next) => {
  const booking = req.body.booking;
  const sevaId = _.get(booking, 'seva');
  const organizationId = _.get(req, 'params.organizationId');

  if (!sevaId) {
    res.status(500).send({
      error: {
        code: 'SEVA_REQUIRED'
      }
    })
    return;
  }

  try {
    const seva = await Seva.findById(sevaId);
    if (!seva) {
      res.status(500).send({
        error: {
          code: 'INVALID_SEVA_ID'
        }
      })
      return;
    };

    if (seva.organization != organizationId) {
      res.status(500).send({
        error: {
          code: 'WHY_U_HACK_ME'
        }
      })
      return;
    }

    const bookingToSave = {
      organization: organizationId,
      seva: sevaId,
      userDetails: booking.userDetails
    };
    const bookingId = _.get(req, 'params.bookingId');

    var createdBooking;

    if (bookingId) {
      // UPDATE
      createdBooking = await Booking.findOneAndUpdate(
        { _id: bookingId },
        {
          ...bookingToSave
        },
        { new: true, overwrite: true }
      );
    } else {
      // CREATE
      createdBooking = await Booking.create(bookingToSave);
    }
    await createdBooking.populate('seva').execPopulate();
    await createdBooking.populate('organization').execPopulate();
    console.log('createdBooking', createdBooking);

    res.send({
      data: createdBooking
    });

  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.remove = async (req, res, next) => {
  const id = req.params.id;

  try {
    const booking = await Booking.findByIdAndRemove(id);
    res.send({
      data: booking
    });
  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}


exports.charge = async (req, res, next) => {
  const {
    token,
    amount,
    stripeCardData
  } = req.body;
  const tokenId = token.id;
  const {
    organizationId,
    bookingId
  } = req.params;

  // console.log('tokenId ', tokenId);
  // console.log('token ', token);
  // console.log('amount ', amount);
  // console.log('stripeCardData ', stripeCardData);

  if (!bookingId) {
    res.status(500).send({
      error: {
        code: 'BOOKING_ID_REQUIRED'
      }
    })
    return;
  };

  try {
    const booking = await Booking.findById(bookingId)
      .populate('seva')
      .populate('organization')
      .exec();

    if (!booking) {
      res.status(500).send({
        error: {
          code: 'INVALID_BOOKING_ID'
        }
      })
      return;
    };

    let charge;
    try {
      charge = await createCharge({
        amount,
        currency: 'usd',
        description: _.get(booking, 'seva.name'),
        source: tokenId
      })

    } catch(e) {
      console.log('e', e);
      // Creted charge successfully
      booking.paymentStatus = {
        status: 'CHARGED_FAILED',
        charge: _.get(e.raw.charge),
        error: {
          message: e.message
        }
      }
      await booking.save();

      res.status(500).send({
        error: {
          code: 'STRIPE_CHARGE_FAILED',
          message: e.message,
          error: e
        }
      })
      return;
    };

    // Creted charge successfully
    booking.paymentStatus = {
      status: 'CHARGED_SUCCESSFULLY',
      charge: charge.id,
      amount: charge.amount
    }

    // Updte BOOKING object
    await booking.save();

    // res.send({
    //   data: charge
    // });

    res.send({
      data: {
        booking
      }
    })

  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.markAsComplete = async (req, res, next) => {
  const { note } = req.body;
  const {
    organizationId,
    bookingId
  } = req.params;

  if (!bookingId) {
    res.status(500).send({
      error: {
        code: 'BOOKING_ID_REQUIRED'
      }
    })
    return;
  };

  try {
    const booking = await Booking.findById(bookingId)
      .populate('seva')
      .populate('organization')
      .exec();

    if (!booking) {
      res.status(500).send({
        error: {
          code: 'INVALID_BOOKING_ID'
        }
      })
      return;
    };

    booking.bookingStatus = booking.bookingStatus || {};
    booking.bookingStatus.status = 'COMPLETE'
    booking.bookingStatus.updatedAt = new Date();
    booking.bookingStatus.note = note;

    // Updte BOOKING object
    await booking.save();

    res.send({
      data: {
        booking
      }
    })

  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}









