export function parseCents(cents) {
  return {
    dollars: Math.floor(cents / 100),
    cents: (cents % 100)
  }
}

export function formatCurrency(cents) {
  const value = parseCents(cents);

  let centsString = `${value.cents}`;
  if (centsString.length == 1) {
    centsString = `${centsString}0`;
  }

  return `$ ${value.dollars}.${centsString}`;
}
