import {
  ADMIN_FETCH_ORGANIZATIONS_SUCCESS,
  ADMIN_FETCH_ORGANIZATIONS_ERROR,
  ADMIN_FETCH_SEVAS_SUCCESS,
  ADMIN_FETCH_SEVAS_ERROR,
  ADMIN_FORM_ORGANIZATION_VALUE_CHANGE,
  ADMIN_FETCH_BOOKINGS_SUCCESS,
  ADMIN_FETCH_BOOKINGS_ERROR
} from './actionsAdmin';
import _get from 'lodash/get';
import _set from 'lodash/set';

const defaultState = {
  organization: null,
  sevas: []
};

export default function admin(state = defaultState, action) {
  switch(action.type) {
    case ADMIN_FETCH_ORGANIZATIONS_SUCCESS: {
      const organization = _get(action, 'payload.data');
      console.log('action', action);

      return {
        ...state,
        organization
      }

    }

    case ADMIN_FETCH_ORGANIZATIONS_ERROR: {
      console.log('NOT_IMPLEMENTED ADMIN_FETCH_ORGANIZATIONS_ERROR');
      return {
        ...state
      }
    }

    case ADMIN_FETCH_SEVAS_SUCCESS: {
      const sevas = _get(action, 'payload.data');
      return {
        ...state,
        sevas
      }

    }
    case ADMIN_FETCH_SEVAS_ERROR: {
      console.log('NOT_IMPLEMENTED ADMIN_FETCH_SEVAS_ERROR');
      return {
        ...state
      }
    }

    case ADMIN_FORM_ORGANIZATION_VALUE_CHANGE: {
      const {
        fieldName,
        fieldValue
      } = action.payload;

      const newState = {
        ...state
      };
      _set(newState, `organization.${fieldName}`, fieldValue);
      return newState;
    }

    case ADMIN_FETCH_BOOKINGS_SUCCESS: {
      const bookings = _get(action, 'payload.data');
      return {
        ...state,
        bookings
      }
    }

    case ADMIN_FETCH_BOOKINGS_ERROR: {
      return {
        ...state
      }
    }

    default: return state;
  }
}