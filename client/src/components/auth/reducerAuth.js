import {
  AUTH_LOGIN_REQUEST,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_ERROR,
  AUTH_REGISTER_REQUEST,
  AUTH_REGISTER_SUCCESS,
  AUTH_REGISTER_ERROR,
  AUTH_BUSY,
  AUTH_LOGOUT_SUCCESS,
  // AUTH_LOGOUT_ERROR,
  AUTH_RETRIEVEUSER_SUCCESS,
  AUTH_RETRIEVEUSER_ERROR
} from './actionsAuth';

const defaultState = {
  busy: true,
  error: null,
  user: null
};

export default function auth(state = defaultState, action) {
  switch(action.type) {
    case AUTH_BUSY: {
      return {
        ...state,
        busy: true
      }
    }

    case AUTH_LOGIN_REQUEST: {
      return {
        ...state,
        busy: true
      }
    }

    case AUTH_RETRIEVEUSER_SUCCESS:
    case AUTH_LOGIN_SUCCESS:
    case AUTH_REGISTER_SUCCESS: {
      const { payload = {} } = action;
      return {
        ...state,
        busy: false,
        error: null,
        authenticated: !!payload.user,
        user: payload.user
      }
    }

    case AUTH_RETRIEVEUSER_ERROR:
    case AUTH_LOGIN_ERROR:
    case AUTH_REGISTER_ERROR: {
      const { payload = {} } = action;
      return {
        ...state,
        busy: false,
        authenticated: false,
        error: payload.error
      };
    }

    case AUTH_REGISTER_REQUEST: {
      return {
        ...state,
        busy: true
      }
    }

    // case AUTH_LOGOUT_ERROR:
    case AUTH_LOGOUT_SUCCESS: {
      return {
        ...state,
        busy: false,
        error: null,
        user: null,
        authenticated: false
      }
    }

    default: return state;
  }
}
