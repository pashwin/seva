import config from '../config';
import _ from 'lodash';
var express = require('express');
var path = require('path');
var logger = require('morgan');
var compression = require('compression');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var dotenv = require('dotenv');
var jwt = require('jsonwebtoken');
var moment = require('moment');
var request = require('request');

var User = require('./models/User');
// Load environment variables from  .env file
dotenv.load();

// Database stuff
var databaseSetup = require('./db/databaseSetup');
var mongoDb;

databaseSetup.connectMongoose()
  .then((results) => {
    console.log('Connection to Mongoose SUCCESS');
  }, (error) => {
    console.log('Connection to Mongoose FAILED');
  });

// Routes
var serverSideRoutes = require('./routes');

var app = express();

app.set('port', process.env.PORT || 3001);
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(cookieParser());

app.use(function(req, res, next) {
  req.isAuthenticated = function() {
    var token = req.headers.authorization && req.headers.authorization.split(' ')[1];
    if (!token) {
      token = req.cookies.token && req.cookies.token.split(' ')[1];
    }

    try {
      const jwtVerify = jwt.verify(token, config.secret);
      return jwtVerify;
    } catch (err) {
      return false;
    }
  };

  var payload = req.isAuthenticated();

  if (payload) {
    User.findById(payload._id).then(function(user) {
      req.user = user;
      next();
    }, function(error) {
      console.log("error", error);
      next();
    });
  } else {
    next();
  }

  req.getUserId = function() {
    return _.get(req, 'user._id');
  }
});

// Add all routes
console.log('process.env.NODE_ENV', process.env.NODE_ENV);
// Add static route before adding other routes
if (process.env.NODE_ENV == 'production') {
  console.log('__dirname', __dirname);
  app.use(express.static(path.join(__dirname, 'build')));
} else {
  console.log('path', path.join(__dirname, '..', 'client', 'static', 'public'));
  app.use(express.static(path.join(__dirname, '..', 'client', 'public', 'static')));
}
// Add API and index.html route
serverSideRoutes.addRoutes(app);

app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});

process.on('uncaughtException', (err) => {
  console.log("uncaughtException");
  console.log(err)
});

process.on('unhandledRejection', function(reason, p){
    console.log("Possibly Unhandled Rejection at");
    console.log("reason", reason);
    console.log("p", p);
});

module.exports = app;
