const defaultOptions = {
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  credentials: "same-origin",
};

export const getOptions = {
  ...defaultOptions,
  method: 'GET'
}

export const postOptions = {
  ...defaultOptions,
  method: 'POST'
}
export const putOptions = {
  ...defaultOptions,
  method: 'PUT'
}

export const deleteOptions = {
  ...defaultOptions,
  method: 'DELETE'
}
