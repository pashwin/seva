import React, { Component } from 'react';

class GlobalError extends Component {
  render() {
    return (
      <div className="error">
        {this.props.error}
      </div>
    );
  }
}
export default GlobalError;

