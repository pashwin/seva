import React, { Component } from 'react';

class IconAndText extends Component {
  render() {
    let onClickFn = this.props.onClick;
    if (!onClickFn) {
      onClickFn = () => {}
    }
    return (
      <div className="icon-and-text">
        <div className="icon">
          <i
            className={`fa ${this.props.icon}`}
            aria-hidden="true"
            onClick={onClickFn}
          ></i>
        </div>
        <p className="text"><small>{this.props.text}</small></p>
      </div>
    );
  }
}
export default IconAndText;