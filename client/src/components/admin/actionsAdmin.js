import {
  getOptions,
  postOptions,
  putOptions,
  deleteOptions
} from '../../api/fetchOptions';
import _get from 'lodash/get';

export const ADMIN_FETCH_ORGANIZATIONS_SUCCESS = 'ADMIN_FETCH_ORGANIZATIONS_SUCCESS';
export const ADMIN_FETCH_ORGANIZATIONS_ERROR = 'ADMIN_FETCH_ORGANIZATIONS_ERROR';
export const ADMIN_FETCH_SEVAS_SUCCESS = 'ADMIN_FETCH_SEVAS_SUCCESS';
export const ADMIN_FETCH_SEVAS_ERROR = 'ADMIN_FETCH_SEVAS_ERROR';
export const ADMIN_FORM_ORGANIZATION_VALUE_CHANGE = 'ADMIN_FORM_ORGANIZATION_VALUE_CHANGE';
export const ADMIN_BUSY = 'ADMIN_BUSY';
export const ADMIN_FETCH_BOOKINGS_SUCCESS = 'ADMIN_FETCH_BOOKINGS_SUCCESS';
export const ADMIN_FETCH_BOOKINGS_ERROR = 'ADMIN_FETCH_BOOKINGS_ERROR';
export const ADMIN_MARK_COMPLETE_SUCCESS = 'ADMIN_MARK_COMPLETE_SUCCESS';
export const ADMIN_MARK_COMPLETE_ERROR = 'ADMIN_MARK_COMPLETE_ERROR';

const busy = () => ({ type: ADMIN_BUSY })

export function fetchOrganizations(organization) {
  return (dispatch, getState) => {
    dispatch(busy());

    const url = '/api/organizations'
    return fetch(url, {
      ...getOptions
    })
    .then((response) => {
      if (!response.ok) {
        return response.json().then(err => {throw err});
      }
      return response.json();
    })
    .then((sevas) => {
      dispatch({
        type: ADMIN_FETCH_ORGANIZATIONS_SUCCESS,
        payload: sevas
      })
      return sevas;
    })
    .catch( (error) => {
      dispatch({
        type: ADMIN_FETCH_ORGANIZATIONS_ERROR
      });
    });
  }
}

export const fetchSevas = (organizationId) => {
  if (!organizationId) {
    console.log('Fetch sevas called without organizationId');
    return;
  }
  return (dispatch, getState) => {
    dispatch(busy());

    const url = `/api/organizations/${organizationId}/sevas`
    return fetch(url, {
      ...getOptions
    })
    .then((response) => {
      if (!response.ok) {
        return response.json().then(err => {throw err});
      }
      return response.json();
    })
    .then((sevas) => {
      dispatch({
        type: ADMIN_FETCH_SEVAS_SUCCESS,
        payload: sevas
      })
      return sevas;
    })
    .catch( (error) => {
      dispatch({
        type: ADMIN_FETCH_SEVAS_ERROR
      });
    });
  }

}

export function saveOrganization(organization) {
  return (dispatch, getState) => {
    dispatch(busy());

    const organizationId = _get(organization, '_id');
    let url = '/api/organizations'
    if (organizationId) {
      url = url + '/' + organizationId;
    }

    return fetch(url, {
      ...( organizationId ? putOptions : postOptions ),
      body: JSON.stringify({
        organization
      })
    })
    .then((response) => {
      if (!response.ok) {
        return response.json().then(err => {throw err});
      }
      return response.json();
    })
    .then((savedOrganization) => {
      dispatch({
        type: ADMIN_FETCH_ORGANIZATIONS_SUCCESS,
        payload: savedOrganization
      })
      return savedOrganization;
    })
    .catch( (error) => {
      dispatch({
        type: ADMIN_FETCH_ORGANIZATIONS_ERROR
      });
    });
  }
}

export function onOrganizationValueChange(fieldValue, fieldName) {
  return {
    type: ADMIN_FORM_ORGANIZATION_VALUE_CHANGE,
    payload: {
      fieldName,
      fieldValue
    }
  }
}

export function saveSeva(organizationId, sevaDetails) {
  return (dispatch, getState) => {
    dispatch(busy());
    const sevaId = sevaDetails._id;
    let url = `/api/organizations/${organizationId}/sevas`
    if (sevaId) {
      url = url + '/' + sevaDetails;
    }

    const seva = {
      ...sevaDetails,
      organization: organizationId
    };

    return fetch(url, {
      ...( sevaId ? putOptions : postOptions ),
      body: JSON.stringify({
        seva
      })
    })
    .then((response) => {
      if (!response.ok) {
        return response.json().then(err => {throw err});
      }
      return response.json();
    })
    .then((savedSeva) => {
      console.log('savedSeva', savedSeva);
      const organizationId = _get(savedSeva, 'data.organization._id');
      dispatch(fetchSevas(organizationId));
      // dispatch({
      //   type: ADMIN_FETCH_ORGANIZATIONS_SUCCESS,
      //   payload: savedSeva
      // })
      return savedSeva;
    })
    .catch( (error) => {
      // dispatch(globalError(error));
      // dispatch({
      //   type: ADMIN_FETCH_ORGANIZATIONS_ERROR
      // });
    });
  }
}

export function deleteSeva(organizationId, sevaDetails) {
  return (dispatch, getState) => {
    dispatch(busy());
    const sevaId = sevaDetails._id;
    let url = `/api/organizations/${organizationId}/sevas`
    if (sevaId) {
      url = url + '/' + sevaId;
    } else {
      console.log('Seva ID is REQUIRED for deleting seva');
      return;
    }

    return fetch(url, {
      ...deleteOptions
    })
    .then((response) => {
      if (!response.ok) {
        return response.json().then(err => {throw err});
      }
      return response.json();
    })
    .then((deletedSeva) => {
      const organizationId = _get(deletedSeva, 'data.organization._id');
      dispatch(fetchSevas(organizationId));
      // dispatch({
      //   type: ADMIN_FETCH_ORGANIZATIONS_SUCCESS,
      //   payload: deletedSeva
      // })
      return deletedSeva;
    })
    .catch( (error) => {
      // dispatch(globalError(error));
      // dispatch({
      //   type: ADMIN_FETCH_ORGANIZATIONS_ERROR
      // });
    });
  }
}

export const fetchBookings = (organizationId) => {
  if (!organizationId) {
    console.log('Fetch bookings called without organizationId');
    return {
      type: ADMIN_FETCH_BOOKINGS_ERROR
    };
  }
  return (dispatch, getState) => {
    dispatch(busy());

    const url = `/api/organizations/${organizationId}/paidbookings`
    return fetch(url, {
      ...getOptions
    })
    .then((response) => {
      if (!response.ok) {
        return response.json().then(err => {throw err});
      }
      return response.json();
    })
    .then((bookings) => {
      dispatch({
        type: ADMIN_FETCH_BOOKINGS_SUCCESS,
        payload: bookings
      })
      return bookings;
    })
    .catch( (error) => {
      dispatch({
        type: ADMIN_FETCH_BOOKINGS_ERROR
      });
    });
  }
}


export const markAsCompleted = (booking) => {
  const organizationId = _get(booking, 'organization._id');
  const bookingId = booking._id;
  if (!organizationId || !bookingId) {
    return {
      type: ADMIN_MARK_COMPLETE_ERROR
    }
  }
  return (dispatch, getState) => {
    dispatch(busy());

    const url = `/api/organizations/${organizationId}/bookings/${bookingId}/markAsComplete`
    return fetch(url, {
      ...postOptions,
      body: JSON.stringify({
        note: _get(booking, 'bookingStatus.note')
      })
    })
    .then((response) => {
      if (!response.ok) {
        return response.json().then(err => {throw err});
      }
      return response.json();
    })
    .then((bookings) => {
      dispatch({
        type: ADMIN_MARK_COMPLETE_SUCCESS,
        payload: bookings
      })
      return bookings;
    })
    .catch( (error) => {
      dispatch({
        type: ADMIN_MARK_COMPLETE_ERROR
      });
    });
  }
}