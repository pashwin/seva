import React, { Component } from 'react';

class VerticalEllipsis extends Component {
  render() {
    return (
      <div className="vertical-ellipsis">
      <i className="fa fa-bars" aria-hidden="true"></i>
      </div>
    );
  }
}
export default VerticalEllipsis;
