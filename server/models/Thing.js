const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Thing;

if (!Thing) {
  const affiliateAccountSchema = new Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    createdAt: Date,
    updatedAt: Date
  },
  {
    timestamps: true
  });

  Thing = mongoose.model('Thing', affiliateAccountSchema);
}

module.exports = Thing;
