import React, { Component } from 'react';
import SevaItem from './SevaItem';
import _map from 'lodash/map';
import _get from 'lodash/get';
import { formatCurrency } from '../../utils/currencyUtils';
import { Button } from 'reactstrap';
import Collapsible from '../common/collapsible/Collapsible';

class SevaList extends Component {
  isSevaSelected = (item) => {
    return _get(this.props, 'selectedSeva._id') === item._id;
  }

  continueToNextAction = () => {
    this.props.continueToNextAction('sevaList');
  }

  renderSevaItems = (sevas) => {
    const subTitle = "All sevas will be performed at first available time";

    return (
      <div>
        <div className="text-muted">{subTitle}</div>
        <div className="seva-items">
          {
            _map(sevas, (seva) => {
              const isSelected = this.isSevaSelected(seva);

              return (
                <div key={seva._id}>
                  <SevaItem
                    key={seva._id}
                    seva={seva}
                    isSelected={isSelected}
                    selectSeva={this.props.selectSeva}
                  />
                  {
                    isSelected
                    ?  <div className="next-action-container">
                        <Button
                          color="danger"
                          size="sm"
                          onClick={this.continueToNextAction}
                        >
                          Continue
                        </Button>
                      </div>
                    : null
                  }
                </div>
              )
            })
          }
        </div>
      </div>
    )
  }

  renderSelectedSeva = () => {
    const selectedSeva = this.props.selectedSeva;
    if (!selectedSeva) return;

    return (
      <div
          className={`seva-item`}
          onClick={this.onClick}
        >
          <div className="column-1">
            <div className="name">{selectedSeva.name}</div>
            <div className="text-muted">{formatCurrency(selectedSeva.price)}</div>
          </div>
        </div>
    )
  }

  render() {
    const { sevas, collapsed } = this.props;

    return (

      <div className="seva-list">
        <Collapsible
          title="Select Seva"
          stepNumber="1"
          collapsed={collapsed}
          expandedComponent={this.renderSevaItems(sevas)}
          collapsedComponent={this.renderSelectedSeva()}
          onExpand={() => this.props.expandCurrentAction('sevaList')}
        >
        </Collapsible>

      </div>
    );
  }
}

export default SevaList;
