import React, { Component } from 'react';
import Home from '../components/home/Home';
import { withRouter } from "react-router-dom";

class HomePage extends Component {
  render() {
    return (
      <div>
        Home page
        <Home />
      </div>
    );
  }
}
export default withRouter(HomePage);
