import {
  // INIT_ONLINE_SEVA,
  SEVA_BUSY,
  FETCH_SEVAS_SUCCESS,
  FETCH_SEVAS_ERROR,
  SET_ACTIVE_MENU_ITEM,
  SELECT_SEVA,
  SAVE_SEVA_DETAILS,
  SET_BOOKING_IN_VIEW,
  SET_COLLAPSED_MENU_ITEM,
  CONTINUE_TO_NEXT_MENU,
  EXPAND_CURRENT_MENU,
  menuItems
} from './actionsOnlineSeva';
import _get from 'lodash/get';
import _set from 'lodash/set';

const defaultState = {
  busy: false,
  error: null,
  user: null,
  activeMenuItem: 'sevaList',
  menuStates: {
    sevaList: {
      collapsed: false,
      expanded: true
    },
    sevaDetails: {
      collapsed: true,
      expanded: false
    },
    paymentDetails: {
      collapsed: true,
      expanded: false
    }
  }
};

export default function onlineSeva(state = defaultState, action) {
  switch(action.type) {
    case SEVA_BUSY: {
      return {
        ...state,
        busy: true,
        error: null
      }
    }

    case FETCH_SEVAS_SUCCESS: {
      const sevas = _get(action, 'payload.data');

      // @TODO: Get this from somewhere else
      const organization = _get(sevas, '[0].organization')

      return {
        ...state,
        organization,
        sevas,
        busy: false,
        error: null
      }
    }

    case FETCH_SEVAS_ERROR: {
      return {
        ...state,
        busy: false,
        error: true
      }
    }

    case SET_ACTIVE_MENU_ITEM: {
      return {
        ...state,
        activeMenuItem: action.payload
      }
    }

    case SELECT_SEVA: {
      const booking = state.booking || {};
      booking.seva = action.payload;

      return {
        ...state,
        booking
      }
    }

    case SET_BOOKING_IN_VIEW: {
      const booking = action.payload;
      return {
        ...state,
        booking
      }
    }

    case SAVE_SEVA_DETAILS: {
      const sevaUserDetails = action.payload;
      const booking = state.booking || {};
      booking.userDetails = sevaUserDetails;

      // const {
      //   name,
      //   dob,
      //   nakshtra,
      //   rashi,
      //   messageToPriest
      // } = sevaUserDetails;

      return {
        ...state,
        booking
      }
    }

    case SET_COLLAPSED_MENU_ITEM: {
      const { menuName } = action.payload;
      const menuStates = state.menuStates;
      menuStates[menuName] = menuStates[menuName] || {};
      menuStates[menuName].collapsed = true;
      menuStates[menuName].expanded = false;

      return {
        ...state,
        menuStates
      }

    }

    case CONTINUE_TO_NEXT_MENU: {
      const { menuName } = action.payload;
      const menuStates = state.menuStates;
      // FIRST - Collapse all of them
      closeAllMenus(menuStates);

      // THEN - open the one required
      openNextMenu(menuStates, menuName);

      return {
        ...state,
        ...menuStates
      }
    }
    case EXPAND_CURRENT_MENU: {
      const { menuName } = action.payload;
      const menuStates = state.menuStates;
      // FIRST - Collapse all of them
      closeAllMenus(menuStates);

      // THEN - open the one required
      menuStates[menuName] = menuStates[menuName] || {};
      menuStates[menuName].collapsed = false;
      menuStates[menuName].expanded = true;
      return {
        ...state,
       ...menuStates
      }
    }


    default: return state;
  }
}

function closeAllMenus(menuStates) {
  for (let i = 0; i < menuItems.length; i++) {
    const menuState = menuItems[i] || {};
    _set(menuStates, `${menuState}.collapsed`, true)
    _set(menuStates, `${menuState}.expanded`, false)
  }

  return menuStates;
}


function openNextMenu(menuStates, menuName) {
  const nextMenuName = getNextMenuItem(menuName);

  if (nextMenuName) {
    menuStates[nextMenuName] = menuStates[nextMenuName] || {};
    menuStates[nextMenuName].expanded = true;
    menuStates[nextMenuName].collapsed = false;
  }

  return menuStates;
}

function getNextMenuItem(menuName) {
  const menuIndex = menuItems.indexOf(menuName);
  if (menuIndex !== -1 && menuItems.length > menuIndex) {
    return menuItems[menuIndex + 1];
  }
  return null;
}

