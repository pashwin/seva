import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  login,
  logout
} from './actionsAuth';
import { withRouter } from "react-router-dom";
import _get from 'lodash/get';

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    login,
    logout
  }, dispatch);
};
class Login extends Component {

  handleKeyPress = (event) => {
    if ( event.charCode === 13 ) {
      this.handleSubmit();
    }
  }

  handleSubmit = () => {
    const email = this.emailInput.value;
    const password = this.passwordInput.value;

    if (!email || !password ) {
      alert('Email and Password are required');
      return;
    }
    this.props.login(email, password)
      .then((results) => {
        console.log("results", results);
        if (this.isLoginPage()) {
          this.props.history.push('/');
        }
      }, (error) => {
        console.log("error", error);
      });
  }

  onSignupLinkClick = () => {
    if (this.props.onSignupLinkClick) {
      this.props.onSignupLinkClick();
      return;
    }

    this.props.history.push('/signup');
  }

  isLoginPage() {
    return _get(this.props, 'location.pathname') === '/login';
  }

  render() {
    const isLoginPage = this.isLoginPage();

    return (
      <div>
        { isLoginPage && <div style={{ height: '32px' }}></div>}
        <div className="auth-form-container">
          <div className="auth-form primary">
            <h3>Login</h3>
            <Form>
              <FormGroup>
                <Label for="email">Email</Label>
                <Input
                  type="email"
                  name="email"
                  id="email"
                  placeholder="Email"
                  innerRef={(input) => (this.emailInput = input)}
                  onKeyPress={this.handleKeyPress}
                />
              </FormGroup>
              <FormGroup>
                <Label for="password">Password</Label>
                <Input
                  type="password"
                  name="password"
                  id="password"
                  innerRef={(input) => (this.passwordInput = input)}
                  onKeyPress={this.handleKeyPress}
                />
              </FormGroup>

              <Button
                block
                color="primary"
                type="button"
                onClick={this.handleSubmit}
              >Login</Button>

              <div style={{ paddingTop: '16px' }}>
                <p
                  style={{ marginBottom: 0 }}
                  className="text-danger">Don't have an account?</p>
                <Button
                  type="button"
                  color="link"
                  onClick={this.onSignupLinkClick}
                  style={{ paddingLeft: 0}}
                ><u>Free Signup</u></Button>
              </div>

            </Form>
          </div>
        </div>
      </div>
    );
  }
}

const LoginWithRouter = withRouter(Login);
export default connect(null, mapDispatchToProps)(LoginWithRouter);

