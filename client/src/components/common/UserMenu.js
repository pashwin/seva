import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { withRouter } from "react-router-dom";
import { Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, NavItem } from 'reactstrap';
import _isEmpty from 'lodash/isEmpty';
import _get from 'lodash/get';
import { logout } from '../auth/actionsAuth';

const mapStateToProps = (state) => {
  return {
    user: _get(state, 'auth.user')
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    logout
  }, dispatch);
};

class UserMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDropdownOpen: false
    };

    this.closeDropDown = this.closeDropDown.bind(this);
  }

  isUserLoggedIn() {
    return !_isEmpty(this.props.user);
  }

  toggleDropdown() {
    this.setState({
      isDropdownOpen: !this.state.isDropdownOpen
    });
  }

  logout = () => {
    this.props.logout();
    this.closeDropDown();
  }

  closeDropDown() {
    this.toggleDropdown();
  }

  renderUserDetails = () => {
    return (
      <NavItem className="ml-auto">
        <Dropdown isOpen={this.state.isDropdownOpen} toggle={this.toggleDropdown.bind(this)}>
          <DropdownToggle
            nav
            color="link"
            caret
          >
            {this.props.user.email}
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem onClick={this.logout}>Logout</DropdownItem>
            <DropdownItem onClick={this.closeDropDown}>Profile</DropdownItem>
          </DropdownMenu>
        </Dropdown>
      </NavItem>
    );
  }

  goToLoginPage = () => {
    this.props.history.push('/login');
  }

  goToSignUpPage = () => {
    this.props.history.push('/signup');
  }

  renderSignInSignUpBtns() {
    return (
      <NavItem className="ml-auto">
        <Button type="button" color="link" onClick={this.goToLoginPage}><u>Log in</u></Button>
        <Button type="button" color="link" onClick={this.goToSignUpPage}><u>Sign up</u></Button>
      </NavItem>
    );
  }

  render() {
    const isUserLoggedIn = this.isUserLoggedIn();
    if (isUserLoggedIn) {
      return this.renderUserDetails();
    } else {
      return this.renderSignInSignUpBtns();
    }

  }
}


const UserMenuWithRouter = withRouter(UserMenu);
export default connect(mapStateToProps, mapDispatchToProps)(UserMenuWithRouter);
