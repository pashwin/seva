import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, FormFeedback, Label, Input } from 'reactstrap';
import _get from 'lodash/get';
import _keys from 'lodash/keys';
import _each from 'lodash/each';

class AddorCreateSeva extends Component {
  state = {
    modalOpen: true,
    name: '',
    price: '',
    validationErrors: {}
  }

  componentDidMount() {
    const { sevaDetails } = this.props;

    this.setState({
      ...sevaDetails
    })
  }

  toggle = () => {
    this.setState({
      modalOpen: !this.state.modalOpen
    });
  }

  saveSeva = () => {
    const validationErrors = {};
    validationErrors.name = this.validateSevaName();
    validationErrors.price = this.validateSevaPrice();

    const isFormValid = this.isFormValid(validationErrors);

    this.setState({
      validationErrors,
      isFormValid
    });

    if (isFormValid) {
      const sevaDetails = {
        name: this.state['name'],
        price: this.state['price']
      };
      this.props.saveSeva(sevaDetails)
    }

  }


  onSevaValueChange = (event, fieldName) => {
    this.setState({
      [fieldName]: event.target.value
    })
  }

  validateSevaName = () => {
    const name = this.state['name']
    if (!name) {
      return {
        valid: false,
        message: 'Name is required'
      }
    }

    if (name.length > 500 ) {
      return {
        valid: false,
        message: 'Name is too long'
      }
    }
  }

  validateSevaPrice = () => {
    const price = this.state['price'];
    if (!price) {
      return {
        valid: false,
        message: 'Price is required'
      }
    }

    if (isNaN(price)) {
      return {
        valid: false,
        message: 'Enter a valid number'
      }
    }
  }

  isFormValid = (validationErrors) => {
    let hasErrors = false;
    const keys = _keys(validationErrors);
    _each(keys, (key) => {
      if (validationErrors[key]) {
        hasErrors = true;
      }
    });
    return !hasErrors;
  }

  renderAddSevaModal = () => {
    const validationErrors = this.state.validationErrors;
    const isNameValid = _get(validationErrors, 'name.valid', true);
    const isPriceValid = _get(validationErrors, 'price.valid', true);
    const {
      name,
      price
    } = this.state;

    return (
      <Modal isOpen={this.state.modalOpen} toggle={this.toggle}>
        <ModalHeader toggle={this.toggle}>Add Seva</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="name">Seva Name</Label>
              <Input
                value={name}
                onChange={(event) => { this.onSevaValueChange(event, 'name')}}
                innerRef={(input) => (this.organizationName = input)}
                type="text"
                name="name"
                id="name"
                placeholder="E.g.: Ganahoma"
                invalid={!isNameValid}
              />
            <FormFeedback>{_get(validationErrors, 'name.message')}</FormFeedback>
            </FormGroup>
            <FormGroup>
              <Label for="price">Price</Label>
              <Input
                value={price}
                onChange={(event) => { this.onSevaValueChange(event, 'price')}}
                innerRef={(input) => (this.organizationAddress = input)}
                type="text"
                name="price"
                id="price"
                placeholder="11.50"
                invalid={!isPriceValid}
              />
            <FormFeedback>{_get(validationErrors, 'price.message')}</FormFeedback>
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.saveSeva}>Save</Button>{' '}
          <Button color="secondary" onClick={this.toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
    )
  }


  render() {
    return (
      <div className="what-the">
        {this.renderAddSevaModal()}
      </div>
    )
  }
}
export default AddorCreateSeva;
