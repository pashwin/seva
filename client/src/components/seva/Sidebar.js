import React, { Component } from 'react';

class Sidebar extends Component {
  state = {
    menuItems: ['sevaList', 'sevaDetails', 'paymentDetails']
  }

  onMenuClick = (menuItem) => {
    this.props.setActiveMenuItem(menuItem);
  }

  render() {
    return (
      <div className="sidebar">
        <div className="menu-container">
          <div
            className="menu-item"
            key={this.state.menuItems[0]}
            onClick={() => this.onMenuClick(this.state.menuItems[0])}
          >
            <span>Seva</span>
          </div>
          <div
            className="menu-item"
            key={this.state.menuItems[1]}
            onClick={() => this.onMenuClick(this.state.menuItems[1])}
          >
            <span>Details</span>
          </div>
          <div
            className="menu-item"
            key={this.state.menuItems[2]}
            onClick={() => this.onMenuClick(this.state.menuItems[2])}
          >
            <span>Payment</span>
          </div>
        </div>
      </div>
    );
  }
}

export default Sidebar;
