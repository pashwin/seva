const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Booking;

if (!Booking) {
  const bookingSchema = new Schema({
    organization: { type: Schema.Types.ObjectId, ref: 'Organization' },
    seva: { type: Schema.Types.ObjectId, ref: 'Seva' },
    userDetails: {
      name: { type: String },
      dob: { type: String },
      nakshatra: { type: String },
      rashi: { type: String },
      messageToPriest: { type: String }
    },
    bookingStatus: { type: Schema.Types.Mixed },
    paymentStatus: { type: Schema.Types.Mixed },
    createdAt: Date,
    updatedAt: Date
  },
  {
    timestamps: true
  });

  Booking = mongoose.model('Booking', bookingSchema);
}

module.exports = Booking;
