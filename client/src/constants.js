const constants = {
  ORGANIZATION_ID: '5b010f83e72753509b943181',
  DOMAIN_BASE: 'http://localhost:3000',
  STRIPE: {
    API_KEY: process.env.STRIPE_PUBLIC_KEY // pk_test_
  }
}

export default constants;
