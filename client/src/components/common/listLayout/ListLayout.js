import React, { Component } from 'react';
import _map from 'lodash/map';

class ListLayout extends Component {
  render() {
    const elements = this.props.elements;

    return (
      <div className="list-layout">
        {
          _map(elements, (element, index) => {
            const isCollapsed = this.props.selectedIndex === index;
            // console.log('isCollapsed', isCollapsed);
            // console.log('this.props.selectedIndex', this.props.selectedIndex);
            // console.log('index', index);
            const elementClasses = `list-element ${isCollapsed ? 'collapsed' : 'expanded'}`

            return (
              <div key={index} className={elementClasses}>
                {element}
              </div>
            )
          })
        }
      </div>
    );
  }
}
export default ListLayout;
