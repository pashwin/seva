const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Organization;

if (!Organization) {
  const organizationSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    name: { type: String, required: true },
    address: { type: String, required: true },
    createdAt: Date,
    updatedAt: Date
  },
  {
    timestamps: true
  });

  Organization = mongoose.model('Organization', organizationSchema);
}

module.exports = Organization;
