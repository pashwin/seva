function createIframeLauncher() {
  var iFrame = document.createElement('iframe');
  iFrame.id = "bookings-launcher";
  iFrame.width = "90";
  iFrame.height = "50";
  iFrame.frameBorder = "0";
  iFrame.src = "http://localhost:3001/iframe-launcher.html";
  document.body.appendChild(iFrame);
}

function addCss(fileName) {

  var head = document.head;
  var link = document.createElement("link");

  link.type = "text/css";
  link.rel = "stylesheet";
  link.href = fileName;

  head.appendChild(link);
}

function launcherEventListener(event) {
  var data = event.data || {};
  var eventName = data.name;
  if (!eventName) {
    return;
  }

  if (eventName === 'book-seva-click') {
    launchBookSevaApp();
  }
}

function launchBookSevaApp() {
  var iFrame = document.createElement('iframe');
  iFrame.id = "bookings-app";
  iFrame.width = window.innerWidth * 0.60;
  iFrame.height = window.innerHeight * 0.60;
  // iFrame.frameBorder = "0";
  iFrame.src = "http://localhost:3000/bookings";
  document.body.appendChild(iFrame);
}

window.onload = function () {
  createIframeLauncher();
  addCss('http://localhost:3001/launcher.css');


  window.addEventListener("message", launcherEventListener, false);
};




console.log('createIframeLauncher Loaded');