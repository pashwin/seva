import React, { Component } from 'react';


class Collapsible extends Component {
  render() {
    const {
      title,
      stepNumber,
      collapsed,
      expandedComponent,
      collapsedComponent
    } = this.props;

    return (
      <div className={`collapsible-container ${collapsed ? 'collapsed' : 'expanded'}`}>
        <div className="header" onClick={this.props.onExpand}>
          <div className="step-number">
            {stepNumber}
          </div>
          <div className="title">
            <h3>{title}</h3>
          </div>
          {
            collapsed
            ? <div className="action-button" onClick={this.props.onExpand}>
              <i className="fas fa-chevron-down"></i>
            </div>
            : null
          }
        </div>
        <div className="collapsible-content">
          {
            collapsed
            ? collapsedComponent
            : expandedComponent
          }
        </div>
      </div>
    );
  }
}
export default Collapsible;
/**
 * {
          collapsed
          ? null
          : {children}
        }
 */