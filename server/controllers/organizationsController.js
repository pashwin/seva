var Organization = require('../models/Organization.js');
const _ = require('lodash');

// const userId = '5aee9d9f5be9bdee0c26f630';

exports.list = async (req, res, next) => {
  try {
    const userId = _.get(req.user, '_id');
    const organizations = await Organization.findOne({
      user: userId
    });

    res.send({
      data: organizations
    });

  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.get = async (req, res, next) => {
  const organizationId = req.params.id;

  try {
    const organization = await Organization.findById(organizationId);

    res.send({
      data: organization
    });


  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.createOrUpdate = async (req, res, next) => {
  const userId = _.get(req.user, '_id');
  const organization = req.body.organization;
  if (_.isEmpty(organization)) {
    res.status(500).send({
      error: {
        code: 'INVALID_DATA'
      }
    })
    return;
  }

  try {
    const organizationToSave = {
      ...organization,
      user: userId
    };
    delete organizationToSave._id;

    var createdOrganization;
    if (organization._id) {
      // UPDATE
      createdOrganization = await Organization.findOneAndUpdate(
        { _id: organization },
        organizationToSave,
        { new: true, overwrite: true }
      )
    } else {
      // CREATE
      createdOrganization = await Organization.create(organizationToSave);
    }

    res.send({
      data: createdOrganization
    });

  } catch (e) {
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

// exports.post = async (req, res, next) => {
//   const organization = req.body.organization;

//   try {
//     const createdOrg = await Organization.create(organization);

//     res.send({
//       data: createdOrg
//     });

//   } catch (e) {
//     console.log('e', e);
//     res.status(500).send({
//       error: {
//         code: 'INTERNAL_SERVER_ERROR'
//       }
//     })
//   }
// }

exports.remove = async (req, res, next) => {
  const id = req.params.id;

  try {
    const organization = await Organization.findByIdAndRemove(id);
    res.send({
      data: organization
    });
  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}











