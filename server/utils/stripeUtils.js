const stripe = require("stripe")("sk_test_PXX8ANd9YwhRHDrfbZxd4QYt");
const _ = require('lodash');


/**
 * chargeParams should contain
 *   amount,
 *   currency: 'usd',
 *   description: '',
 *   source: tokenId
 *
 */
export async function createCharge(chargeParams) {
  try {
    const charge = await stripe.charges.create(chargeParams);
    return charge;

  } catch(e) {
    throw e;
  }
}
