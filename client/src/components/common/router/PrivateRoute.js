import React from 'react';
import {
  Route,
  Redirect
} from 'react-router-dom';
import { connect } from 'react-redux';

const mapStateToProps = (state, ownProps) => {
    return {
        auth: state.auth
    };
};


const PrivateRouteComponent = ({ component: Component, auth, ...rest }) => {

  auth = auth || {};
  if (auth.busy) {
    return <div>Loading ... </div>
  }

  return (
    <Route {...rest} render={(props) => {
      if (auth.user) {
        return <Component {...props} />
      }
      return <Redirect to={{
          pathname: '/login',
          state: { from: props.location }
        }} />
      }}
    />

  )
}

const PrivateRoute = connect(mapStateToProps, null)(PrivateRouteComponent);
export default PrivateRoute;
