import React, { Component } from 'react';
import AddorCreateSeva from './AddorCreateSeva';
import AdminSevaList from './AdminSevaList';
import { Button } from 'reactstrap';
import _isEmpty from 'lodash/isEmpty';

class AdminSevas extends Component {
  state = {
    isModalOpen: false
  }
  // componentWillReceiveProps(newProps) {
  //   const { sevaDetails } = newProps;

  //   this.setState({
  //     ...sevaDetails
  //   })
  // }

  renderSevas = () => {
    return <AdminSevaList
      sevas={this.props.sevas}
      addSeva={this.addSeva}
      deleteSeva={this.props.deleteSeva}
    />
  }

  addSeva = () => {
    this.setState({
      isModalOpen: true
    })

  }

  saveSeva = (sevaDetails) => {
    return this.props.saveSeva(sevaDetails)
      .then((results) => {
        console.log('results', results);
        this.setState({
          isModalOpen: false
        })
      })
      .catch((error) => {
        console.log('error ', error);
      });
  }

  renderEmptySevasView = () => {
    return (
      <div className="empty-sevas">
        <h3 className="heading">Add sevas that can be booked by devotees</h3>
        <Button color="danger" onClick={this.addSeva}>Add Seva</Button>

      </div>
    )
  }

  renderAddOrCreateSevas = () => {
    return (
      <AddorCreateSeva
        saveSeva={this.saveSeva}
      />
    )
  }

  render() {
    const sevasEmpty = _isEmpty(this.props.sevas);
    return (
      <div>
        <h3>Seva Details</h3>
        {/*
          _isEmpty(this.props.sevas)
          ? this.renderEmptySevasView()
          : this.renderSevas()
        */}
        {sevasEmpty && this.renderEmptySevasView()}
        {!sevasEmpty && this.renderSevas()}
        {this.state.isModalOpen && this.renderAddOrCreateSevas()}
      </div>
    );
  }
}
export default AdminSevas;
