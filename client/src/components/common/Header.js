import React from 'react';
import { Navbar, NavbarBrand, Nav, NavItem, NavbarToggler, Collapse } from 'reactstrap';
import UserMenu from './UserMenu';
import { connect } from 'react-redux';

const mapStateToProps = (state, ownProps) => {
  return {
    appProjects: state.appProjects
  };
}

class Header extends React.Component {
  state = {
    isHamburgerMenuOpen: false
  };

  handleLogout(event) {
    event.preventDefault();
    // this.props.dispatch(logout());
  }

  renderUserDetails() {
    return <UserMenu />
  }

  toggleNavBar = () => {
    this.setState({
      isHamburgerMenuOpen: !this.state.isHamburgerMenuOpen
    })
  }

  renderHeader() {
    return (
      <div className="app-navbar-container" >
        <Navbar color="primary" dark expand="md">
          <NavbarBrand href="/">Online Seva</NavbarBrand>
          <Nav style={{ position: 'absolute', left: '40%' }}>
            <NavItem>
              <span className="navbar-text">
                  APP_HEADER
                </span>
            </NavItem>
          </Nav>
          <NavbarToggler onClick={this.toggleNavBar} />
          <Collapse isOpen={this.state.isHamburgerMenuOpen} navbar>
            <Nav className="ml-auto" navbar>
              {this.renderUserDetails()}
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }

  render() {
    return this.renderHeader();
  }
}

export default connect(mapStateToProps, null)(Header);