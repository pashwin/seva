const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Seva;

if (!Seva) {
  const sevaSchema = new Schema({
    organization: { type: Schema.Types.ObjectId, ref: 'Organization' },
    name: { type: String, required: true },
    price: { type: Number, required: true },
    createdAt: Date,
    updatedAt: Date
  },
  {
    timestamps: true
  });

  Seva = mongoose.model('Seva', sevaSchema);
}

module.exports = Seva;
