import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  register
} from './actionsAuth';
import { withRouter } from "react-router-dom";
import _get from 'lodash/get';

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    register
  }, dispatch);
};
class Signup extends Component {

  handleKeyPress = (event) => {
    if(event.charCode === 13){
      this.handleSubmit();
    }
  }

  handleSubmit = () => {
    const email = this.emailInput.value;
    const password = this.passwordInput.value;

    if (!email || !password) {
      alert('Name, Email and Password are required');
      return;
    }
    this.props.register(email, password)
      .then((results) => {
        console.log("results", results);
        if (this.isSignupPage()) {
          this.props.history.push('/');
        }
      }, (error) => {
        console.log("error", error);
      });
  }

  onLoginLinkClick = () => {
    if (this.props.onLoginLinkClick) {
      this.props.onLoginLinkClick();
      return;
    }

    this.props.history.push('/login');
  }

  isSignupPage() {
    return _get(this.props, 'location.pathname') === '/signup';
  }

  render() {
    const isSignupPage = this.isSignupPage();
    return (
      <div>
        { isSignupPage && <div style={{ height: '32px' }}></div>}
        <div className="auth-form-container">
          <div className="auth-form primary">
            <h3>Signup</h3>
            <Form>
              <FormGroup>
                <Label for="email">Email</Label>
                <Input
                  type="email"
                  name="email"
                  id="email"
                  placeholder="Email"
                  innerRef={(input) => (this.emailInput = input)}
                  onKeyPress={this.handleKeyPress}
                />
              </FormGroup>
              <FormGroup>
                <Label for="password">Password</Label>
                <Input
                  type="password"
                  name="password"
                  id="password"
                  innerRef={(input) => (this.passwordInput = input)}
                  onKeyPress={this.handleKeyPress}
                />
              </FormGroup>

              <Button
                block
                color="info"
                type="button"
                onClick={this.handleSubmit}
              >Signup</Button>

              <div style={{ paddingTop: '16px' }}>
                <p
                  style={{ marginBottom: 0 }}
                  className="text-danger">Already have an account?</p>
                <Button
                  type="button"
                  color="link"
                  onClick={this.onLoginLinkClick}
                  style={{ paddingLeft: 0}}
                ><u>Login Now</u></Button>
              </div>

            </Form>
          </div>
        </div>
      </div>
    );
  }
}

const SignupWithRouter = withRouter(Signup);
export default connect(null, mapDispatchToProps)(SignupWithRouter);

