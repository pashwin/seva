// @flow
import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import auth from '../components/auth/reducerAuth';
import admin from '../components/admin/reducerAdmin';
import onlineSeva from '../components/seva/reducerOnlineSeva';

const rootReducer = combineReducers({
  auth,
  onlineSeva,
  admin,
  router
});

export default rootReducer;
