import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

class AddressSection extends Component {
  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        <FormGroup>
          <Label for="name">Name on the card</Label>
          <Input type="text" name="name-on-card" id="name-on-card" />
        </FormGroup>
        <FormGroup>
          <Label for="exampleEmail">Address Line 1</Label>
          <Input type="text" name="address1" id="address1" />
        </FormGroup>
        <FormGroup>
          <Label for="exampleEmail">Address Line 2</Label>
          <Input type="text" name="address2" id="address2" />
        </FormGroup>
        <FormGroup>
          <Label for="exampleEmail">City</Label>
          <Input type="text" name="city" id="city" />
        </FormGroup>
        <FormGroup>
          <Label for="exampleEmail">State</Label>
          <Input type="text" name="state" id="state" />
        </FormGroup>
        <FormGroup>
          <Label for="exampleEmail">Zip</Label>
          <Input type="text" name="zip" id="zip" />
        </FormGroup>
      </Form>
    );
  }
}
export default AddressSection;
