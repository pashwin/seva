import React from 'react';
import ReactDOM from 'react-dom';
import Root from './containers/Root';
import { store, history } from './store/configureStore.dev.js';

import './styles/index.css';

ReactDOM.render(
  <Root store={store} history={history} />,
  document.getElementById('root')
);

// import registerServiceWorker from './registerServiceWorker';
// registerServiceWorker();
