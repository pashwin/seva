var Thing = require('../models/Thing.js');


exports.list = async (req, res, next) => {
  try {
    const things = await Thing.find();

    res.send({
      data: things
    });

  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.get = async (req, res, next) => {
  const thingId = req.params.id;

  try {
    const thing = await Thing.findById(thingId);

    res.send({
      data: thing
    });


  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.post = async (req, res, next) => {
  const thing = req.body.thing;

  try {
    const createdThing = await Thing.create(thing);

    res.send({
      data: createdThing
    });

  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}

exports.remove = async (req, res, next) => {
  const id = req.params.id;

  try {
    const thing = await Thing.findByIdAndRemove(id);
    res.send({
      data: thing
    });
  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}











