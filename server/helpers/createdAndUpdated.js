export function setCreatedAndUpdatedAt(obj) {
  var d = Date.now();
  obj.updatedAt = d;
  obj.createdAt = obj.createdAt || d;
}

