import React, { Component } from 'react';

class Callout extends Component {
  render() {
    const {
      color = 'info',
      header,
      text
    } = this.props;
    return (
      <div className={`bs-callout bs-callout-${color}`}>
        <h4>{header}</h4>
        {text}
      </div>
    );
  }
}
export default Callout;
