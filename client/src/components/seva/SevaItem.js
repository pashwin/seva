import React, { Component } from 'react';
import { formatCurrency } from '../../utils/currencyUtils';

class SevaItem extends Component {
  onClick = (e) => {
    this.props.selectSeva(this.props.seva);
  }

  render() {
    const { isSelected } = this.props;

    return (
      <div
        className={`seva-item ${isSelected ? 'selected': ''}`}
        onClick={this.onClick}
      >
        <div className="column-2">
          <i className="fa fa-check" aria-hidden="true"></i>
        </div>
        <div className="column-1">
          <div className="name">{this.props.seva.name}</div>
          <div className="price">{formatCurrency(this.props.seva.price)}</div>
        </div>
      </div>
    );
  }
}

export default SevaItem;
