import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  fetchBookings,
  markAsCompleted
} from './actionsAdmin';
import { Container, Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, FormFeedback } from 'reactstrap';
import _get from 'lodash/get';
import _map from 'lodash/map';

const mapStateToProps = (state, ownProps) => {
  return {
    admin: state.admin
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchBookings,
    markAsCompleted
  }, dispatch);
};

class AdminBookings extends Component {
  state = {
    isMarkAsCompletedModalOpen: false,
    selectedBooking: null
  }

  componentDidMount() {
    this.props.fetchBookings(this.props.organization._id);
  }

  markAsCompleted = () => {
    this.props.markAsCompleted(this.state.selectedBooking)
      .then((results) => {
        console.log('results', results);
        this.toggle();
        this.props.fetchBookings(this.props.organization._id);
      })
      .catch((error) => {
        console.log('error ', error);
        this.toggle();
        this.props.fetchBookings(this.props.organization._id);
      });
  }

  openMarkAsCompletedModal = (booking) => {
    this.setState({
      isMarkAsCompletedModalOpen: true,
      selectedBooking: booking
    })
  }

  toggle = () => {
    this.setState({
      isMarkAsCompletedModalOpen: !this.state.isMarkAsCompletedModalOpen,
      selectedBooking: null
    })
  }

  onNoteChange = (event) => {
    const selectedBooking = this.state.selectedBooking;
    selectedBooking.bookingStatus = selectedBooking.bookingStatus || {};
    selectedBooking.bookingStatus.note = event.target.value;
    this.setState({
      selectedBooking
    })
  }

  renderMarkAsCompletedModal = () => {
    const isNameValid = true;
    const validationErrors = {};
    const note = _get(this.state, 'selectedBooking.bookingStatus.note', '');

    return (
      <Modal isOpen={this.state.isMarkAsCompletedModalOpen} toggle={this.toggle}>
        <ModalHeader toggle={this.toggle}>Mark as Completed</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="name">Message to Devotee</Label>
              <Input
                onChange={this.onNoteChange}
                value={note}
                type="textarea"
                name="name"
                id="name"
                placeholder="Thank you for offering your seva. We have completed it today."
                invalid={!isNameValid}
              />
            <FormFeedback>{_get(validationErrors, 'note.message')}</FormFeedback>
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={() => this.markAsCompleted()}>OK</Button>{' '}
          <Button color="secondary" onClick={this.toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
    )
  }

  render() {
    const bookings = _get(this.props, 'admin.bookings');
    return (
      <Container>
        <h3>Seva Bookings</h3>

        <div className="admin-booking-list">
          {
            _map(bookings, (booking) => {
              const date = new Date(_get(booking, 'createdAt'));
              const formattedDate = date.toLocaleString();
              const isComplete = _get(booking, 'bookingStatus.status', '') === 'COMPLETE';

              return (
                <div className="admin-booking-list-item" key={booking._id}>
                  <Row>
                    <Col xs="12" sm="3" className="key">Name</Col>
                    <Col xs="12" sm="9" className="value"><div>{_get(booking, 'seva.name')}</div></Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="3" className="key">Date</Col>
                    <Col xs="12" sm="9" className="value"><div>{formattedDate}</div></Col>
                  </Row>
                  <h5>Devotee Details</h5>
                  <Row>
                    <Col xs="12" sm="3" className="key">Name</Col>
                    <Col xs="12" sm="9" className="value"><div>{_get(booking, 'userDetails.name')}</div></Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="3" className="key">Nakshatra</Col>
                    <Col xs="12" sm="9" className="value"><div>{_get(booking, 'userDetails.nakshatra')}</div></Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="3" className="key">Rashi</Col>
                    <Col xs="12" sm="9" className="value"><div>{_get(booking, 'userDetails.rashi')}</div></Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="3" className="key">MessageToPriest</Col>
                    <Col xs="12" sm="9" className="value"><div>{_get(booking, 'userDetails.messageToPriest')}</div></Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="3" className="key">Date of birth</Col>
                    <Col xs="12" sm="9" className="value"><div>{_get(booking, 'userDetails.dob')}</div></Col>
                  </Row>

                  <Row>
                    <Col xs="12">
                      {
                        isComplete
                        ? <Button
                            disabled
                            color="secondary"
                          >Completed
                        </Button>
                        : <Button
                            color="danger"
                            onClick={() => this.openMarkAsCompletedModal(booking)}
                          >Mark as Completed
                        </Button>
                      }
                    </Col>
                  </Row>



                </div>
              )
            })
          }
          {
            this.state.isMarkAsCompletedModalOpen
            ? this.renderMarkAsCompletedModal()
            : null
          }
        </div>




      </Container>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminBookings);
