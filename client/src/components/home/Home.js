import { Container } from 'reactstrap';
import { connect } from 'react-redux';
import React, { Component } from 'react';

import Callout from '../common/callout/Callout';
import Login from '../auth/Login';
import Signup from '../auth/Signup';

const mapStateToProps = (state, ownProps) => {
  return {
    auth: state.auth
  };
}

class Home extends Component {
  state = {
    formToShow: 'login',
    borderColor: 'primary'
  }

  onSignupLinkClick = () => {
    this.setState({
      formToShow: 'signup',
      borderColor: 'info'
    });
  }
  onLoginLinkClick = () => {
    this.setState({
      formToShow: 'login',
      borderColor: 'primary'
    });
  }

  renderUnauthenticated() {
    return (
      <div>
        <Callout
          color='primary'
          header='Access Restricted'
          text='Please login to view this page'
        />
        {
          this.state.formToShow === 'login' ?
          <Login
            onSignupLinkClick={this.onSignupLinkClick}
          /> :
          <Signup
            onLoginLinkClick={this.onLoginLinkClick}
          />
        }
      </div>
    );
  }

  renderAuthenticated() {
    return <div>Authenticated</div>;
  }

  render() {
    const { auth } = this.props;
    const authenticated = auth.authenticated ? 'YES' : 'NO';
    return (
      <div className="home">
        <Container>
          {
            authenticated === 'NO' ?
            this.renderUnauthenticated() :
            this.renderAuthenticated()
          }
        </Container>
      </div>
    );
  }
}
export default connect(mapStateToProps, null)(Home);
