import React, { Component } from 'react';
import constants from '../../constants';

class IFrameEmbedInfo extends Component {

  getIframeString = () => {
    const {
      organizationId
    } = this.props;

    var str = `<iframe
  src="${constants.DOMAIN_BASE}/organizations/${organizationId}/bookings"
  width='100%'
  height=600
  frameBorder=0
/>`;
    return str;
  }

  render() {
    return (
      <div>
        <h3>Iframe Details</h3>
        <pre className="pre">{this.getIframeString()}
        </pre>
      </div>
    );
  }
}
export default IFrameEmbedInfo;
