import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import OnlineSeva from '../components/seva/OnlineSeva';

const styles = {
  launcher: {
    'background': 'orange',
    'border-radius': '12px',
    'width': '80px',
    'height': '40px',
    'display': 'inline-block',
    'cursor': 'pointer'
  },
  text: {
    'color': 'black',
    'padding': '4px',
    'display': 'inline-block'
  }

};

class Launcher extends Component {
  state = {
    launchApp: false
  };

  launchBookingApp = () => {
    console.log('launchBookingApp')
    this.setState({
      launchApp: !this.state.launchApp
    })

  }

  renderApp = () => {
    console.log('renderApp');
    return (
      <Modal isOpen={this.state.launchApp} toggle={this.launchBookingApp} className={this.props.className}>
        <ModalHeader toggle={this.launchBookingApp}>Modal title</ModalHeader>
        <ModalBody>
          <OnlineSeva />
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.launchBookingApp}>Do Something</Button>{' '}
          <Button color="secondary" onClick={this.launchBookingApp}>Cancel</Button>
        </ModalFooter>
      </Modal>
    )
  }

  render() {
    return (
      <div
        id="launcher"
        style={styles.launcher}
        onClick={this.launchBookingApp}
      >
        {
          !this.state.launchApp
          ? <div style={styles.text} >
              Book Seva
            </div>
          : this.renderApp()
        }
      </div>
    );
  }
}

export default Launcher;
