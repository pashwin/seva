import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  setActiveMenuItem,
  fetchSevas,
  selectSeva,
  saveSevaUserDetails,
  saveStripeToken,
  fetchBooking,
  saveBooking,
  setCollapsedMenuItem,
  continueToNextMenuItem,
  expandCurrentAction,
  setBookingInView
} from './actionsOnlineSeva';
import ListLayout from '../common/listLayout/ListLayout'
import SevaList from './SevaList';
import SevaDetails from './SevaDetails';
import Confirmation from './Confirmation';
import PaymentDetails from '../payment/PaymentDetails';
import _get from 'lodash/get';
import { withRouter } from 'react-router';

const mapStateToProps = (state, ownProps) => {
  return {
    onlineSeva: state.onlineSeva
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    setActiveMenuItem,
    fetchSevas,
    selectSeva,
    saveSevaUserDetails,
    saveStripeToken,
    fetchBooking,
    saveBooking,
    setCollapsedMenuItem,
    continueToNextMenuItem,
    expandCurrentAction,
    setBookingInView
  }, dispatch);
};

class OnlineSeva extends Component {
  constructor(props) {
    super(props);
    this.menuItems = ['sevaList', 'sevaDetails', 'paymentDetails' ];
  }

  componentDidMount() {

    // Fetch all Sevas
    this.props.fetchSevas()
      .then((results) => {
        this.props.setActiveMenuItem('sevaList');
      })
      .catch((error) => {
        console.log('error ', error);
      });
    // Fetch Booking
    const bookingId = _get(this.props, 'match.params.bookingId');
    if (bookingId) {
      this.props.fetchBooking('5ad0c1f1978d3f71e9659d18', bookingId);
    }
  }

  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  setActiveMenuItem = (menuName) => {
    this.props.setActiveMenuItem(menuName);
  }

  selectSeva = (seva) => {
    this.props.selectSeva(seva);
  }

  saveSevaUserDetails = (details) => {
    const organization = _get(this.props, 'onlineSeva.organization');
    this.props.saveSevaUserDetails(organization,  details);
    this.saveBooking(details);
  }

  saveBooking = (details) => {
    const booking = _get(this.props, 'onlineSeva.booking');
    const organization = _get(this.props, 'onlineSeva.organization');

    return this.props.saveBooking(organization, booking)
      .then((results) => {
        const bookindId = _get(results, 'data._id');
        this.props.history.push(`/bookings/${bookindId}`);
        this.continueToNextAction('sevaDetails');
      })
      .catch((error) => {
        console.log('error ', error);
      });

  }

  expandCurrentAction = (menuName) => {
    this.props.expandCurrentAction(menuName);
  }

  continueToNextAction = (menuName) => {
    this.props.continueToNextMenuItem(menuName);
  }

  saveStripeToken = (token, stripeCardData) => {
    const organizationId = _get(this.props, 'onlineSeva.booking.organization._id');
    const bookingId = _get(this.props, 'onlineSeva.booking._id');

    return this.props.saveStripeToken(organizationId, bookingId, token, stripeCardData)
      .then((updatedBooking) => {
        this.props.setBookingInView(_get(updatedBooking, 'data.booking'));
      })
      .catch((error) => {
        console.log('error ', error);
      });
  }

  renderSevaList = () => {
    const {
      sevas,
      booking = {},
      menuStates
    } = this.props.onlineSeva;
    const sevaListMenuState = menuStates['sevaList'] || {};

    return <SevaList
      sevas={sevas}
      selectedSeva={booking.seva}
      selectSeva={this.selectSeva}
      collapsed={sevaListMenuState.collapsed}
      continueToNextAction={this.continueToNextAction}
      expandCurrentAction={this.expandCurrentAction}
    />
  }

  renderSevaDetails = () => {
    const sevaDetails = _get(this.props, 'onlineSeva.booking.userDetails');
    const collapsed = _get(this.props, 'onlineSeva.menuStates.sevaDetails.collapsed', true);

    return <SevaDetails
      sevaDetails={sevaDetails}
      saveSevaUserDetails={this.saveSevaUserDetails}
      collapsed={collapsed}
      continueToNextAction={this.continueToNextAction}
      expandCurrentAction={this.expandCurrentAction}
    />
  }

  renderPaymentDetails = () => {
    const selectedSevaPrice = _get(this.props, 'onlineSeva.booking.seva.price');
    const paymentStatus = _get(this.props, 'onlineSeva.booking.paymentStatus');
    const collapsed = _get(this.props, 'onlineSeva.menuStates.paymentDetails.collapsed', true);

    return <PaymentDetails
      saveStripeToken={this.saveStripeToken}
      chargeAmount={selectedSevaPrice}
      paymentStatus={paymentStatus}
      collapsed={collapsed}
      continueToNextAction={this.continueToNextAction}
      expandCurrentAction={this.expandCurrentAction}
    />
  }

  renderContentV2 = () => {
    return (
      <ListLayout
        elements={[
          this.renderSevaList(),
          this.renderSevaDetails(),
          this.renderPaymentDetails()
        ]}
        selectedIndex={1}
      />
    )
  }

  renderConfirmation = () => {
    return (
      <Confirmation booking={this.props.onlineSeva.booking} />
    )
  }

  render() {
    const showConfirmation = _get(this.props, 'onlineSeva.booking.paymentStatus.status') === 'CHARGED_SUCCESSFULLY';
    return (
      <div className="online-seva-container">
        {/*<Sidebar
          collapsed={this.state.collapsed}
          setActiveMenuItem={this.setActiveMenuItem}
        />*/}
        <div className="content" >
          {
            showConfirmation
            ? this.renderConfirmation()
            : this.renderContentV2()
          }
        </div>
      </div>
    );
  }
}

const OnlineSevaWithRouter = withRouter(OnlineSeva);
export default connect(mapStateToProps, mapDispatchToProps)(OnlineSevaWithRouter);

