const path = require('path');
const passport = require('passport');
const passportService = require('./helpers/authHelpers');
const requireLogin = passport.authenticate('local', { session: false });
const requireAuth = passport.authenticate('jwt', { session: false });

const authController = require('./controllers/authController');
const usersController = require('./controllers/usersController');
const things = require('./controllers/thingsController');
const organizations = require('./controllers/organizationsController');
const sevas = require('./controllers/sevasController');
const bookings = require('./controllers/bookingsController');

function addRoutes(app) {
  app.all('*', (req, res, next) => {
    console.log(req.method + ' ' + req.url);
    next();
  });

  // USERS
  app.get('/api/user/getLoggedInUser', requireAuth, usersController.getLoggedInUser)
  // AUTH
  app.post('/api/auth/register', authController.register);
  app.post('/api/auth/login', requireLogin, authController.login);

  app.get('/api/things', requireAuth, things.list);
  app.get('/api/things/:thingId', requireAuth, things.get);
  app.post('/api/things', requireAuth, things.post);
  app.delete('/api/things', requireAuth, things.list);


  app.get('/api/organizations', organizations.list);
  app.get('/api/organizations/:organizationId', organizations.get);
  app.post('/api/organizations', organizations.createOrUpdate);
  app.put('/api/organizations/:organizationId', organizations.createOrUpdate);
  app.delete('/api/organizations', organizations.list);


  // TODO: Convert these to include ORGANIZATIONID
  app.get('/api/organizations/:organizationId/sevas', sevas.list);
  app.get('/api/sevas/:thingId', sevas.get);
  app.post('/api/sevas', sevas.post);
  app.put('/api/organizations/:organizationId/sevas', sevas.createOrUpdate);
  app.post('/api/organizations/:organizationId/sevas', sevas.createOrUpdate);
  app.delete('/api/organizations/:organizationId/sevas/:sevaId', sevas.remove);

  app.get('/api/organizations/:organizationId/bookings', bookings.list);
  app.get('/api/organizations/:organizationId/paidbookings', bookings.paidBookings);
  app.get('/api/organizations/:organizationId/bookings/:bookingId', bookings.get);
  app.post('/api/organizations/:organizationId/bookings', bookings.createOrUpdate);
  app.put('/api/organizations/:organizationId/bookings/:bookingId', bookings.createOrUpdate);
  app.delete('/api/organizations/:organizationId/bookings/:bookingId', bookings.remove);
  app.post('/api/organizations/:organizationId/bookings/:bookingId/charge', bookings.charge);
  app.post('/api/organizations/:organizationId/bookings/:bookingId/markAsComplete', bookings.markAsComplete);

  // app.get('/launcher.js', (req, res) => {
  //   res.sendFile(path.join(__dirname, '..', 'client', 'public', 'static', 'launcher.js'));
  // });

  app.get('/*', (req, res) => {
    console.log('/* route - No other route caught this request');
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
  });

}

const routes = {
  addRoutes: addRoutes
}

module.exports = routes;
