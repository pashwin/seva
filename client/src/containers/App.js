import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getLoggedInUser } from '../components/auth/actionsAuth';

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getLoggedInUser
  }, dispatch);
};

class App extends Component {
  componentDidMount() {
    this.props.getLoggedInUser();
    setTimeout(() => {
      // console.clear();
      // /^(?!.*?getDefaultProps)/
    }, 1000);
  }

  render() {
    return (
      <div>
        { this.props.children }
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(App);
