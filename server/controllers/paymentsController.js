const stripe = require("stripe")(process.env.STRIPE_PRIVATE_KEY);

exports.charge = async (req, res, next) => {
  try {
    const bookingId = _get(req, 'query.bookingId');
    console.log('bookingId', bookingId);
    var {
      token,
      amount,
      stripeCardData
    } = req.body || {};

    amount = 100;

    console.log('token', token);
    console.log('amount', amount);
    console.log('stripeCardData', stripeCardData);

    // Validate all fields here

    const charge = stripe.charges.create({
      amount,
      currency: 'usd',
      description: 'Online Seva Booking',
      source: token,
    });


    res.send({
      data: sevas
    });

  } catch (e) {
    console.log('e', e);
    res.status(500).send({
      error: {
        code: 'INTERNAL_SERVER_ERROR'
      }
    })
  }
}