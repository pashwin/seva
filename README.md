## Temples Seva Booking App

This project provides a mechanism for temples to offer Sevas that can be booked online by devotees.

### For temples

- Create list of available Sevas - Eg: Ganahoma, Ksheera Abhisheka
- Integrate with temple website using `iframe`
- Comes integrated with a payment processor
- Bless the devotee after performing the seva


### For devotees
- Book Seva online
- Pay using credit card
- Receive a note from the priest after the seva is performed.

