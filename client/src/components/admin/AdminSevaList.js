import React, { Component } from 'react';
import { Button } from 'reactstrap';
import IconAndText from '../common/iconAndText/IconAndText';
import { formatCurrency } from '../../utils/currencyUtils';

class AdminSevaList extends Component {
  deleteSeva = (seva) => {
    console.log('seva ', seva);
    this.props.deleteSeva(seva);
  }

  render() {
    const { sevas } = this.props;

    return (
      <div>
        <Button color="danger" onClick={this.props.addSeva}>Add Seva</Button>
        {
          sevas.map((seva) => {
            return (
              <div key={seva._id} className="admin-seva-list">
                <div className="seva-details">
                  <div className="seva-name">
                    {seva.name}
                  </div>
                  <div className="seva-price">
                    {formatCurrency(seva.price)}
                  </div>
                </div>
                <div className="seva-menu">
                  <IconAndText
                    icon="fa-trash"
                    text="Delete"
                    onClick={() => this.deleteSeva(seva)}
                  />
                </div>
              </div>
            )
          })
        }
      </div>
    );
  }
}
export default AdminSevaList;
