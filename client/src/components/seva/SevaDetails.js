import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, FormFeedback } from 'reactstrap';
import _keys from 'lodash/keys';
import _has from 'lodash/has';
import _each from 'lodash/each';
import Collapsible from '../common/collapsible/Collapsible';

class SevaDetails extends Component {
  state = {
    name: '',
    nakshatra: '',
    rashi: '',
    messageToPriest: '',
    dob: '',
    formFields: {
      name: {}
    },
    validationErrors: {}
  }

  componentDidMount() {
    const { sevaDetails } = this.props;

    this.setState({
      ...sevaDetails
    })
  }

  componentWillReceiveProps(newProps) {
    const { sevaDetails } = newProps;

    this.setState({
      ...sevaDetails
    })
  }

  onChange = (fieldName, event) => {
    console.log('event.target.value', event.target.value);
    this.setState({
      [fieldName]: event.target.value
    })
  }

  validateName = () => {
    if (!this.state.name) {
      return {
        valid: false,
        message: 'Name is required'
      }
    }

    if (this.state.name.length > 50 ) {
      return {
        valid: false,
        message: 'Name is too long'
      }
    }
  }

  isFormValid = (validationErrors) => {
    let hasErrors = false;
    const keys = _keys(validationErrors);
    _each(keys, (key) => {
      if (validationErrors[key]) {
        hasErrors = true;
        console.log('validationErrors[key]', validationErrors[key]);
      }
    });
    return !hasErrors;
  }

  isFieldValid = (fieldName) => {
    if (_has(this.state.validationErrors, fieldName)) {
      return !this.state.validationErrors[fieldName];
    }

    return true;
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const validationErrors = {};
    validationErrors.name = this.validateName();

    const isFormValid = this.isFormValid(validationErrors)

    this.setState({
      validationErrors,
      isFormValid
    });

    const sevaUserDetails = {
      name: this.state['name'],
      nakshatra: this.state['nakshatra'],
      rashi: this.state['rashi'],
      messageToPriest: this.state['messageToPriest'],
      dob: this.state['dob']
    };

    if (isFormValid) {
      this.props.saveSevaUserDetails(sevaUserDetails);
    }
  }

  handleFocus = (e, inputName) => {
    console.log('inputName', inputName);
    const validationErrors = this.state.validationErrors;
    validationErrors[inputName] = undefined;
    this.setState({
      validationErrors
    }, () => {
      console.log('this.state.validationErrors', this.state.validationErrors);
    });

  }

  // onExpand = () => {
  //   this.setState({
  //     collapsed: false
  //   })
  // }

  renderDetails = () => {
    return (
      <div>
        <div className="text-muted">Enter all the details of the devotee</div>
        <div style={ { height: '20px' }}></div>
        <Form>
          <FormGroup>
            <Label for="name">Name</Label>
            <Input
              type="text"
              name="name"
              id="name"
              value={this.state.name}
              placeholder=""
              onFocus={(e) => { this.handleFocus(e, 'name') }}
              onChange={(e) => this.onChange('name', e)}
              invalid={!this.isFieldValid('name')}
            />
            {
              !this.isFieldValid('name') && <FormFeedback invalid={true}>Please enter a name</FormFeedback>
            }
            <FormText>Seva will be performed for this name</FormText>
          </FormGroup>
          <FormGroup>
            <Label for="dob">Date of Birth</Label>
            <Input
              type="date"
              name="dob"
              id="dob"
              placeholder=""
              value={this.state.dob}
              onChange={(e) => this.onChange('dob', e)} />
          </FormGroup>
          <FormGroup>
            <Label for="nakshatra">Nakshatra</Label>
            <Input
              type="select"
              name="nakshatra"
              id="nakshatra"
              placeholder=""
              value={this.state.nakshatra}
              onChange={(e) => this.onChange('nakshatra', e)}
            >
              <option value="dont-know">- Please Select -</option>
              <option value="ashvini">Ashvini</option>
              <option value="bharani">Bharani</option>
              <option value="krittika">Krittika</option>
              <option value="rohini">Rohini</option>
              <option value="mrigashirsha">Mrigashirsha</option>
              <option value="ardra">Ardra</option>
              <option value="punarvasu">Punarvasu</option>
              <option value="pushya">Pushya</option>
              <option value="ashlesha">Ashlesha</option>
              <option value="magha">Magha</option>
              <option value="purva">Purva</option>
              <option value="uttara">Uttara</option>
              <option value="hasta">Hasta</option>
              <option value="chitra">Chitra</option>
              <option value="swati">Swati</option>
              <option value="vishakha">Vishakha</option>
              <option value="anuradha">Anuradha</option>
              <option value="jyeshtha">Jyeshtha</option>
              <option value="mula">Mula</option>
              <option value="purva">Purva</option>
              <option value="uttara">Uttara</option>
              <option value="shravana">Shravana</option>
              <option value="dhanishtha">Dhanishtha</option>
              <option value="shatabhisha">Shatabhisha</option>
              <option value="Purva-bhadrapada">Purva Bhadrapada</option>
              <option value="Uttara-bhadrapada">Uttara Bhadrapada</option>
              <option value="revati">Revati</option>
            </Input>

          </FormGroup>
          <FormGroup>
            <Label for="rashi">Rashi</Label>
            <Input
              type="select"
              name="rashi"
              id="rashi"
              placeholder=""
              value={this.state.rashi}
              onChange={(e) => this.onChange('rashi', e)}
            >
              <option value="dont-know">- Please Select -</option>
              <option value="mesha">Mesha</option>
              <option value="vrishabha">Vrishabha</option>
              <option value="mithun">Mithun</option>
              <option value="karka">Karka</option>
              <option value="simha">Simha</option>
              <option value="kanya">Kanya</option>
              <option value="tula">Tula</option>
              <option value="vruschika">Vruschika</option>
              <option value="dhanu">Dhanu</option>
              <option value="makar">Makar</option>
              <option value="kumbha">Kumbha</option>
              <option value="meena">Meena</option>
            </Input>

          </FormGroup>
          <FormGroup>
            <Label for="messageToPriest">Message to the Priest</Label>
            <Input
              type="textarea"
              name="messageToPriest"
              id="messageToPriest"
              placeholder=""
              value={this.state.messageToPriest}
              onChange={(e) => this.onChange('messageToPriest', e)}
            />
            <FormText>Please enter additional information for the priest</FormText>
          </FormGroup>
          <Button
            color="danger"
            onClick={this.handleSubmit}
          >Save Details</Button>
        </Form>
      </div>
    )
  }

  renderDetailsSumary = () => {
    const { sevaDetails } = this.props;
    if (!sevaDetails) return;

    return (
      <div>Details for {sevaDetails.name}</div>
    )
  }

  render() {
    // const { validationErrors } = this.state;

    return (
      <Collapsible
        title="Devotee Details"
        stepNumber="2"
        collapsed={this.props.collapsed}
        expandedComponent={this.renderDetails()}
        collapsedComponent={this.renderDetailsSumary()}
        onExpand={() => this.props.expandCurrentAction('sevaDetails')}
      >
      </Collapsible>
    );
  }
}

export default SevaDetails;
