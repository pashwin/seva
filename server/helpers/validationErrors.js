exports.createErrorString = function(code, message) {
  return {
    error: {
      code,
      message
    }
  }
}

exports.createErrorArray = function(errorArray, code, message) {
  return {
    error: {
      errors: errorArray,
      code,
      message
    }
  }
}
